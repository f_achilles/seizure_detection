%% make matrixGraph
% Same Subject
SameSubMatFig = figure;
for row = 1:size(SameSubjectMatrix,1)
    hold on
    plot(SameSubjectMatrix(row,:),'-+','LineWidth',3)
    hold off
end
grid on
ylabel('AUC in [%]')
title(['Same-Subject Training'])
set(gca,'FontSize',20)
set(gcf,'Position',pos)
legend('N = 1', 'N = 2', 'N = 5', 'N = 10', 'Pisani et al.', 'Location', 'NorthEast')
% axis([0 1 0 1])

%% Cross Subject

CrossSubMatFig = figure;
for row = 1:size(CrossSubjectMatrix,1)
    hold on
    plot(CrossSubjectMatrix(row,:),'-+','LineWidth',3)
    hold off
end
grid on
ylabel('AUC in [%]')
title(['Cross-Subject Training'])
set(gca,'FontSize',20)
set(CrossSubMatFig,'Position',pos)

legend('N = 1', 'N = 2', 'N = 5', 'N = 10', 'Location', 'NorthEast')
% axis([0 1 0 1])

%% draw teaser images

sz5Thr = 0.636;

figure;
bar(find(seizureProbability(1:numel(sz5TestInds))>=sz5Thr ),ones(1,nnz(seizureProbability(1:numel(sz5TestInds))>=sz5Thr )),1,'green','edgecolor','none')
hold on
plot(seizureProbability(1:numel(sz5TestInds)),'LineWidth',2)
plot(1:numel(sz5TestInds),repmat(sz5Thr ,1,numel(sz5TestInds)),'r-','LineWidth',2)
hold off

ylabel('$p(\mathrm{seizure}}$')
set(gca,'FontSize',20)

%
%GT
bar(find(gt(1:numel(sz5TestInds))-1>=sz5Thr ),ones(1,nnz(gt(1:numel(sz5TestInds))-1>=sz5Thr )),1,'green','edgecolor','none')
hold on
plot(gt(1:numel(sz5TestInds))-1,'LineWidth',2)
hold off

set(gcf,'Position',pos)