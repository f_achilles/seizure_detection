% close all
aucPlotPos = [100   100   700   600];

% AUC results of method comparison

% left to right: cnn_best, hogsvm, acfdf
aucMatrix = [78.71 60.39 47.91];

CrossSubMethFig = figure;
subplot(2,1,1)
hold on
bar(1, aucMatrix(1), 'FaceColor', [0 0.75 0])
bar(2, aucMatrix(2), 'FaceColor', [0 0 0.75])
bar(3, aucMatrix(3), 'FaceColor', [0.75 0 0])
hold off

grid on
ylabel('AUC in [%]')

title('Method evaluation')

% set(gca,'FontSize',15)

set(CrossSubMethFig,'Position',aucPlotPos)

set(gca,'XTick',[1 2 3])
% set(gca,'XTickLabel',{'ours (depth+IR (A))','HoG+SVM','ACF-DF'})
set(gca,'XTickLabel',{'ours','HoG+SVM','ACF-DF'})

ylim([40 80])

set(gcf,'color','w')

%
% AUC results of modality comparison
% left to right: depth only, ir only, ir+depth A,  ir+depth B
aucMatrix = [72.61 71.37 78.33 71.80];

subplot(2,1,2)

hold on
bar(1, aucMatrix(1), 'FaceColor', [0 0.25 0])
bar(2, aucMatrix(2), 'FaceColor', [0 0.50 0])
bar(3, aucMatrix(3), 'FaceColor', [0 0.75 0])
bar(4, aucMatrix(4), 'FaceColor', [0 1.00 0])
hold off

grid on
ylabel('AUC in [%]')

title('Input modality evaluation')

set(gca,'FontSize',15)


set(gca,'XTick',[1 2 3 4])
set(gca,'XTickLabel',{'depth','IR','depth+IR (A)','depth+IR (B)'})

ylim([40 80])

export_fig(gcf,'C:\Projects\SD_MICCAI_paper\images\EvalFig','-pdf');


%%
% ROC curve of CNN results

resultsCounter = 1;

networkType = 4;
cLR = 1e-3;
FPR = [];
TPR = [];
for fold = 1:2
    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_depth_Type%d_LR%.2f_fold%d',networkType,log(cLR)/log(10),fold);
    load(fullfile(netDir,'wdwSz1_results_szOnly.mat'));
    
    seizureProbabilityDepth = squeeze(testResults(1,:,2));
    
    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type%d_LR%.2f_fold%d',networkType,log(cLR)/log(10),fold);
    load(fullfile(netDir,'wdwSz1_results_szOnly.mat'));
    
    % multiplication of seizure probabilities
    seizureProbability = seizureProbabilityDepth .* squeeze(testResults(1,:,2));
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
%         accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    
    if isempty(FPR)
        FPR = 1-specificity;
        TPR = recall;
    else
        FPR = (FPR + 1-specificity)/2;
        TPR = (TPR + recall)/2;
    end
end
ROCfig3 = figure;

FPRraw = FPR;
TPRraw = TPR;
[FPR,srtdInds] = sort(FPR);
TPR = TPR(srtdInds);
hold on
hROC = plot(FPR,...
    TPR,...
    'g-', 'lineWidth',3,'markerSize',10);
hold off

% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=TPR(a);
    y=TPR(b);
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end
title('ROC curves')
xlabel('1-specificity'); ylabel('sensitivity');

axis equal
axis([0 1 0 1])
% set(gca,'FontSize',15)
% set(gcf,'Color','w')

%
% ROC curve of HoG+SVM results

FPR = [];
TPR = [];

   
for fold = 1:2
    load(['C:\Projects\SeizureDetection\workshop\SVMresults\crossSubject_depth\fold' num2str(fold) '_results.mat'],...
        'isSeizure','prediction','recall','precision','specificity','set','PostProbs','AUC')
    threshold = 0:0.001:1.001;
    recallCurve = zeros(1,numel(threshold));
    specifCurve = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(isSeizure(set==3) & PostProbs(:,2)>=threshold(iThr));
        TNnum = nnz(~isSeizure(set==3) & PostProbs(:,2)<threshold(iThr));
        recallCurve(iThr) = TPnum / (nnz(isSeizure(set==3))+eps);
        specifCurve(iThr) = TNnum / (nnz(~isSeizure(set==3))+eps);
    end
    if isempty(FPR)
        FPR = 1-specifCurve;
        TPR = recallCurve;
    else
        FPR = (FPR + 1-specifCurve)/2;
        TPR = (TPR + recallCurve)/2;
    end
end
% ROC curve
% ROCfig2 = figure;
hold on
hROC = plot(FPR,...
    TPR,...
    'b-.', 'lineWidth',3,'markerSize',10);
hold off

% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=TPR(a);
    y=TPR(b);
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end

title(['ROC curve of fold ' num2str(fold) ', AUC: ' num2str(-AUC)])
xlabel('1-specificity'); ylabel('sensitivity');
% set(gca,'FontSize',15)
axis equal
axis([0 1 0 1])




%
% ROC curve of Pisani results

FPR = [];
TPR = [];
for fold = 1:2
    load(['C:\Projects\SeizureDetection\workshop\PISresults' filesep 'Pisani_Fold' num2str(fold)],'AUC','ROCCollector','TPPosTNNegCollector')
    if isempty(FPR)
        FPR = ROCCollector(1,:);
        TPR = ROCCollector(2,:);
    else
        FPR = (FPR + ROCCollector(1,:))/2;
        TPR = (TPR + ROCCollector(2,:))/2;
    end
end
% ROC curve
% ROCfig = figure;
hold on
hROC = plot(FPR,...
    TPR,...
    'r--', 'lineWidth',3,'markerSize',10);
hold off

% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=TPR(a);
    y=TPR(b);
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end

title(['ROC curve of fold ' num2str(fold) ', AUC: ' num2str(AUC)])
xlabel('1-specificity'); ylabel('sensitivity');
% set(gca,'FontSize',15)
axis equal
axis([0 1 0 1])

%%
title('ROC curves')
legend('depth+IR (A)','HoG+SVM','ACF-DF')

export_fig(gcf,'C:\Projects\SD_MICCAI_paper\images\ROCFig','-pdf');