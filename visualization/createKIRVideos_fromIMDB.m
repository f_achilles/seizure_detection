%% generate KIR videos from IMDB

% load directory list
patientDir = ['C:\Projects\SeizureDetection\11002_lowRes'];
mkdir(patientDir)
patID = 11002;
n = imdb.meta.dbsizes;
firstSeqInd = find(imdb.meta.patientIDvec(n+1)==patID,1,'first')-1;
if firstSeqInd > 0
    sequences = [find(imdb.meta.patientIDvec(n+1)==patID)];
else
    sequences = [find(imdb.meta.patientIDvec(n+1)==patID)];
end

for iSeq = sequences% 1:(numel(n)-1)
    irVideo             = VideoWriter([patientDir filesep num2str(iSeq) '_irVideo'],'MPEG-4');%'Grayscale AVI');
    irVideo.Quality     = 90; 
    irVideo.FrameRate   = 30;
    open(irVideo)
    for frameInd = (n(iSeq)+1):n(iSeq+1)
        im = imdb.images.data(:,:,1,frameInd);
        im = reshape(max(min(im(:)/255,1),0),100,100);
%         im=rot90(im);
        indexedImg = imresize(im,[424,424],'bilinear');
        indexedImg = insertText(indexedImg,[1 1; 1 30; 1 60],{num2str(imdb.meta.timestamps(frameInd),17) ['Frame ' num2str(frameInd)] ['Sequence ' num2str(iSeq)]});
        writeVideo(irVideo,indexedImg);
    end
    close(irVideo)
    fprintf('Done with sequence %d.\n',iSeq)
end
