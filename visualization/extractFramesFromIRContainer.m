%% extract colored frames

frameInd = 845;

figure;
im = irContainer(:,(256-212+1):(256+212),frameInd);
indexedImg = single(histeq(im, 1000))/(2^16-1);
imagesc(indexedImg); axis image off
colormap(hot)
set(gcf,'Position',pos)

%%
%% extract depth frames

frameInd = 400;

figure;
im = movieContainer(:,:,1,frameInd);
indexedImg = im;
% indexedImg = single(histeq(im, 1000))/(2^16-1);
imagesc(indexedImg); axis image off
colormap(gray)
set(gcf,'Position',[20 100 700 700])
