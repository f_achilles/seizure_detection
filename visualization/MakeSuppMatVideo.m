irVideo = VideoWriter(['IM1226_sz07_depthVideo_fine'],'MPEG-4');%'Grayscale AVI');
irVideo.Quality     = 99;
irVideo.FrameRate   = 30;
cMap    = gray(350);
open(irVideo)
% for kirInd = 1:numel(kirFiles)
% [irContainer, timestamps] = playKIR([patientDir filesep eventDir(eventInd).name filesep kirFiles(kirInd).name]);
% N=numel(timestamps);
N = size(movieContainer,4);
for frameInd = 1:N
%     im = movieContainer(:,:,1,frameInd)/(2^16-1);
    im = movieContainer(:,:,2,frameInd)/4500;
    indexedImg = single(histeq(im,1000));
    indexedImgCol = reshape(cMap(round(indexedImg(:)*256),:),[424 424 3]);
%     indexedImg = insertText(indexedImg,[1 1; 1 30; 1 60],{num2str(timestamps(frameInd),17) ['Frame ' num2str(frameInd)] ['Video ' num2str(kirInd)]});
    writeVideo(irVideo,indexedImgCol);
    % double the frame in order to get a 30fps video
    writeVideo(irVideo,indexedImgCol);
end
% end
close(irVideo)

%% find seizure probability of a certain seizure
n = imdb.meta.dbsizes;
testInds10Frames = find(boolean(imdb.meta.valid10Frames) & (imdb.images.set == 3));
cSz = 8;
figure; plot(imdb.meta.valid10Frames((n(cSz)+1):(n(cSz+1)))); title(['Valid 10 frames of Seizure ' num2str(cSz)]); axis tight

careProbability = squeeze(testResults(2,:,2));

startInd10Frames = find(imdb.meta.valid10Frames((n(cSz)+1):(n(cSz+1))), 1,'first') + n(cSz);
endInd10Frames = find(imdb.meta.valid10Frames((n(cSz)+1):(n(cSz+1))), 1,'last') + n(cSz);
startOfSzInTestSet = find(startInd10Frames==testInds10Frames);
endOfSzInTestSet = find(endInd10Frames==testInds10Frames);
clippedSeizureProb = seizureProbability(startOfSzInTestSet:endOfSzInTestSet);
clippedCareProb    = careProbability(startOfSzInTestSet:endOfSzInTestSet);
figure; plot(clippedSeizureProb); title(['Probability of Seizure ' num2str(cSz) ' at frames with 10 predecessors.'])
figure; plot(gt(1,startOfSzInTestSet:endOfSzInTestSet)); title(['GT of Seizure ' num2str(cSz) ' at frames with 10 predecessors.'])
figure; plot(gtLbls(2,startOfSzInTestSet:endOfSzInTestSet)); title(['GT of Care at sz ' num2str(cSz) ' at frames with 10 predecessors.'])

% movie container is defined as the part that has valid15FPS==1
testInds15FPS = find(boolean(imdb.meta.valid15FPSvec) & (imdb.images.set == 3));
startInd15Frames = find(imdb.meta.valid15FPSvec((n(cSz)+1):(n(cSz+1))), 1,'first') + n(cSz);
endInd15Frames = find(imdb.meta.valid15FPSvec((n(cSz)+1):(n(cSz+1))), 1,'last') + n(cSz);
startOfSz15FPSInTestSet = find(startInd15Frames==testInds15FPS);
endOfSz15FPSInTestSet = find(endInd15Frames==testInds15FPS);

% assign seizureProb to each of the testInds15FPS that are in the certain
% seizure
N15FPS = sum(imdb.meta.valid15FPSvec((n(cSz)+1):(n(cSz+1)))); %size(movieContainer,4);
clippedSeizureProb15FPS = zeros(1,N15FPS);
clippedCareProb15FPS    = zeros(1,N15FPS);
for i15 = 1:N15FPS
    currentIMDBInd = testInds15FPS(i15+startOfSz15FPSInTestSet-1);
    clippedSeizureProb15FPS(i15) = clippedSeizureProb(max(1,min(knnsearch(testInds10Frames',currentIMDBInd)-startOfSzInTestSet+1,numel(clippedSeizureProb))));
    clippedCareProb15FPS(i15)    = clippedCareProb(max(1,min(knnsearch(testInds10Frames',currentIMDBInd)-startOfSzInTestSet+1,numel(clippedCareProb))));
end
figure; plot(clippedSeizureProb15FPS); title(['Probability of Seizure ' num2str(cSz) ' @15FPS'])
figure; plot(clippedCareProb15FPS); title(['Probability of Care in sz ' num2str(cSz) ' @15FPS'])

suppmatFig = figure('Position',[100 100 1280 720]);
videoSub = subplot(4,2,[1 3]);
im = movieContainer(:,:,1,1)/(2^16-1);
indexedImg = single(histeq(im,1000));%/(2^16-1);
indexedImgCol = reshape(cMap(round(indexedImg(:)*256),:),[424 424 3]);
hFrame = imagesc(indexedImgCol); axis image off

thr =  0.56; %0.75 % 0.414;

probPlotSub = figure('Position',[100 100 1280 200],'color','white');
hProbPlot = plot(clippedCareProb15FPS,'k','linewidth',2);
axis([0 numel(clippedCareProb15FPS) 0 1])
% ylabel('\textit{p}(seizure)','Interpreter','latex')
ylabel('\it p\rm(care)','Interpreter','tex')
set(gca,'box','off','fontsize',15)
grid on
hold on
hBar = bar(1,1,4,'blue','edgecolor','none');
thrLine = plot(1:numel(clippedCareProb15FPS),...
    repmat(thr,1,numel(clippedCareProb15FPS)),'r','linewidth',2);
uistack(hProbPlot,'top')
uistack(thrLine,'top')
uistack(hBar,'top')
% make a movie
probVideo = VideoWriter(['sz' num2str(cSz) 'careProbability_DualObj'],'MPEG-4');%'Grayscale AVI');
probVideo.Quality     = 99;
probVideo.FrameRate   = 30;
open(probVideo)
for i=1:numel(clippedCareProb15FPS)
    if i>1 && clippedCareProb15FPS(i-1)>= thr
        if i> 250
            cBarWidth = get(cbar,'BarWidth');
            icBar = get(cbar,'XData');
            set(cbar,'BarWidth',cBarWidth+1,'XData',icBar+0.5);
        else
            cbar = bar(i-1,1,1,'green','edgecolor','none');
        end
        uistack(cbar,'bottom')
    end
    set(hBar,'XData',i)
    
%     uistack(hProbPlot,'top')
%     uistack(thrLine,'top')
%     uistack(hBar,'top')
%     
    writeVideo(probVideo,getframe(probPlotSub));
    writeVideo(probVideo,getframe(probPlotSub));
end
hold off
close(probVideo)

thr =  0.56; %0.75 % 0.414;

% probPlotSub = subplot(4,2,[5 6]);
probPlotSub = figure('Position',[100 100 1280 200],'color','white');
hProbPlot = plot(clippedSeizureProb15FPS,'k','linewidth',2);
axis([0 numel(clippedSeizureProb15FPS) 0 1])
% ylabel('\textit{p}(seizure)','Interpreter','latex')
ylabel('\it p\rm(seizure)','Interpreter','tex')
set(gca,'box','off','fontsize',15)
grid on
hold on
hBar = bar(1,1,4,'blue','edgecolor','none');
thrLine = plot(1:numel(clippedSeizureProb15FPS),...
    repmat(thr,1,numel(clippedSeizureProb15FPS)),'r','linewidth',2);
uistack(hProbPlot,'top')
uistack(thrLine,'top')
uistack(hBar,'top')
% make a movie
probVideo = VideoWriter(['sz' num2str(cSz) 'seizureProbability_DualObj'],'MPEG-4');%'Grayscale AVI');
probVideo.Quality     = 99;
probVideo.FrameRate   = 30;
open(probVideo)
for i=1:numel(clippedSeizureProb15FPS)
    if i>1 && clippedSeizureProb15FPS(i-1)>= thr
        if i> 350
            cBarWidth = get(cbar,'BarWidth');
            icBar = get(cbar,'XData');
            set(cbar,'BarWidth',cBarWidth+1,'XData',icBar+0.5);
        else
            cbar = bar(i-1,1,1,'green','edgecolor','none');
        end
        uistack(cbar,'bottom')
    end
    set(hBar,'XData',i)
    
%     uistack(hProbPlot,'top')
%     uistack(thrLine,'top')
%     uistack(hBar,'top')
%     
    writeVideo(probVideo,getframe(probPlotSub));
    writeVideo(probVideo,getframe(probPlotSub));
end
hold off
close(probVideo)

% GT for Care-Event
probPlotGT = figure('Position',[100 100 1280 200],'color','white'); % = subplot(4,2,[7 8]);
bar(find([zeros(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gtLbls(2,startOfSzInTestSet:endOfSzInTestSet)-1]),...
    ones(1,nnz([zeros(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gtLbls(2,startOfSzInTestSet:endOfSzInTestSet)-1]==1)),...
    1,'facecolor', [0 0.4 0],'edgecolor','none')
axis([0 numel(clippedSeizureProb15FPS) 0 1])
grid off
xlabel('frames \rightarrow')
ylabel('ground truth')
set(gca,'Ytick',[],'box','off','fontsize',15)
hold on
hBar = bar(1,1,4,'blue','edgecolor','none');
% make a movie
gtVideo = VideoWriter(['sz' num2str(cSz) 'careGT_darker_fat'],'MPEG-4');%'Grayscale AVI');
gtVideo.Quality     = 99;
gtVideo.FrameRate   = 30;
open(gtVideo)
for i=1:numel(clippedSeizureProb15FPS)
    set(hBar,'XData',i)
    writeVideo(gtVideo,getframe(probPlotGT));
    writeVideo(gtVideo,getframe(probPlotGT));
end
close(gtVideo)

probPlotGT = figure('Position',[100 100 1280 200],'color','white'); % = subplot(4,2,[7 8]);
% bar(find([zeros(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gt(1,startOfSzInTestSet:endOfSzInTestSet)-1]),ones(1,nnz([zeros(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gt(1,startOfSzInTestSet:endOfSzInTestSet)-1]==1)),1,'facecolor', [0 0.4 0],'edgecolor','none')
bar(find([ones(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gt(1,startOfSzInTestSet:endOfSzInTestSet)-1]),ones(1,nnz([ones(1,numel(clippedSeizureProb15FPS)-numel(clippedSeizureProb)) gt(1,startOfSzInTestSet:endOfSzInTestSet)-1]==1)),1,'facecolor', [0 0.4 0],'edgecolor','none')
axis([0 numel(clippedSeizureProb15FPS) 0 1])
grid off
xlabel('frames \rightarrow')
ylabel('ground truth')
set(gca,'Ytick',[],'box','off','fontsize',15)
hold on
hBar = bar(1,1,4,'blue','edgecolor','none');
% make a movie
gtVideo = VideoWriter(['sz' num2str(cSz) 'seizureGT_darker_fat'],'MPEG-4');%'Grayscale AVI');
gtVideo.Quality     = 99;
gtVideo.FrameRate   = 30;
open(gtVideo)
for i=1:numel(clippedSeizureProb15FPS)
    set(hBar,'XData',i)
    writeVideo(gtVideo,getframe(probPlotGT));
    writeVideo(gtVideo,getframe(probPlotGT));
end
close(gtVideo)



