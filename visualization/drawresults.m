patientNames = {'IM1226', 'IM1237', 'IM11982'};
trainValTestSplits = {{[2 4 6];[3];[1 5]},... %fair: [2 4 6];[3];[1 5]},...
    {[2 4];[3];[1 5]},...
    {[1 2 3 6 10 11 12 15 16 17 22];[4 14 21];[5 7 8 9 13 18 19 20]}};
threshold = 0:0.001:1.001;

ROCfig = figure;
for wdwSz = [1 2 5 10]
    FPRCollector = zeros(1,numel(threshold));
    recallCollector = zeros(1,numel(threshold));
for patientInd = 1 %[1 2 3];
netDir = ['C:\Projects\SeizureDetection\data\networks\validation\medianFilteredDepth\' patientNames{patientInd}];

% load training results

    % SAME SUBJECT
    load([netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_results_szOnly.mat'],'gt','testResults');
    
    Ntest = size(testResults,2);
    seizureProbability = squeeze(testResults(1,:,2));
%     noSeizureProbability = squeeze(testResults(1,:,1));
    
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    %%%
    % Draw ROC-curve
    %%%
%     [FPR,FPRSortInds] = sort(1-specificity);
    FPR = 1-specificity;
    FPRSortInds = 1:numel(FPR);
    
%     [FPR,FPRSortInds] = sort(1-specificity);
    FPRCollector = FPRCollector+FPR;
    recallCollector = recallCollector + recall;
% calc AUC
% AUC=0;
% for a=1:(numel(FPR)-1)
%     b=a+1;
%     x=recall(FPRSortInds(a));
%     y=recall(FPRSortInds(b));
%     AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
% end
% AUC

end
hold on
    rocSame = plot(FPRCollector/3,recallCollector/3,'-.', 'LineWidth', 3);
hold off
end
legend('N = 1', 'N = 2', 'N = 5', 'N = 10', 'Location', 'SouthEast')
title(['Average ROC Curves'])
xlabel('1-specificity'); ylabel('sensitivity');
axis equal
axis([0 1 0 1])
set(gca,'FontSize',20)
grid on