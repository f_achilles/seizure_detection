i=3370;
im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
    if wdwSz > 1
    im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
    end
    if wdwSz > 2
    im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
    im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
    im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
    end
    if wdwSz > 5
    im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
    im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
    im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
    im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
    im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
    end
    if wdwSz > 10
    im11 = (imdb.images.data(:,:,:,testInds(i)-10)-net.meta.meanImage)./net.meta.stdDevImage;
    im12 = (imdb.images.data(:,:,:,testInds(i)-11)-net.meta.meanImage)./net.meta.stdDevImage;
    im13 = (imdb.images.data(:,:,:,testInds(i)-12)-net.meta.meanImage)./net.meta.stdDevImage;
    im14 = (imdb.images.data(:,:,:,testInds(i)-13)-net.meta.meanImage)./net.meta.stdDevImage;
    im15 = (imdb.images.data(:,:,:,testInds(i)-14)-net.meta.meanImage)./net.meta.stdDevImage;
    end
    res = vl_simplenn(net, gpuArray(cat(3,im15,im14,im13,im12,im11,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
    'disableDropout', true, ...
    'conserveMemory', false, ...
    'sync', false) ;
figure
imagesc(histeq(squeeze(imdb.images.data(:,:,:,testInds(3370)))/255));
axis image; colormap(gray);

figure;
for i=1:8, subplot(2,4,i); imagesc(squeeze(res(3).x(:,:,i)));
    axis image; colormap(gray);
end

figure; imagesc(squeeze(res(9).x)); axis image off; colormap(jet)

figure; imagesc(squeeze(res(11).x)); axis image off; colormap(jet)

%% visualize a sequence from frame a to frame b of the test set
% shaking arm then waking up
% a=3898;
% b=4027;

% slight wiggling
% a=3340;
% b=3400;

a=9850;
b=10000;

figure;
for i=a:b
    imagesc(imdb.images.data(:,:,:,testInds(i))); axis image;
    caxis([87 108])
    text(0,10,['frame ' num2str(i)])
    pause(0.06666);
    drawnow
end

imdb.meta.patientIDvec(testInds(i))
