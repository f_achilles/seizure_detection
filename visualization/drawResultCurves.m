%% make curves
% precision-recall curve
% sort, just in case
[~, recallSortInds] = sort(recall);
PRfig = figure; plot(recall(recallSortInds),precision(recallSortInds));
% PRfig = figure; plot(recall,precision);
xlabel('recall'); ylabel('precision');
axis equal
axis([0 1 0 1])
% saveas(PRfig, [netDir filesep num2str(patientNames(patientInd)) '_wdwSz' num2str(wdwSz) '_PRcurve_szOnly'], 'png');
% saveas(PRfig, [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_PRcurve_szOnly'], 'png');
% ROC curve
ROCfig = figure; plot(1-specificity,recall);
% sort, just in case
[FPR,FPRSortInds] = sort(1-specificity);
% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=recall(FPRSortInds(a));
    y=recall(FPRSortInds(b));
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end
title(['ROC curve, AUC: ' num2str(AUC)])
xlabel('1-specificity'); ylabel('sensitivity');
axis equal
axis([0 1 0 1])
% saveas(ROCfig, [netDir filesep num2str(patientNames(patientInd)) '_wdwSz' num2str(wdwSz) '_ROCcurve_szOnly'], 'png');
% saveas(ROCfig, [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_ROCcurve_szOnly'], 'png');
% accuracy curve (just for "fun")
% sort, just in case
ACCfig = figure; plot(threshold,accuracy);
xlabel('threshold'); ylabel('accuracy');
axis equal
axis([0 1 0 1])
% saveas(ACCfig, [netDir filesep num2str(patientNames(patientInd)) '_wdwSz' num2str(wdwSz) '_ACCcurve_szOnly'], 'png');
% saveas(ACCfig, [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_ACCcurve_szOnly'], 'png');
% make colored timeseries
timePlot = figure;
% seizure GT
subplot(2,1,1)
hold on
if any(gt(1,:)==2)
bar(find(gt(1,:)==2),ones(1,nnz(gt(1,:)==2)),1,'green','edgecolor','none')
end
if any(gt(1,:)==1)
bar(find(gt(1,:)==1),ones(1,nnz(gt(1,:)==1)),1,'red','edgecolor','none')
end
axis tight
hold off
title('Ground truth result for event "seizure"')
% seizure test
TPnum = 0;
TNnum = 0;
subplot(2,1,2)
hold on
if any(seizureProbability>noSeizureProbability)
bar(find(seizureProbability>noSeizureProbability),ones(1,nnz(seizureProbability>noSeizureProbability)),1,'green','edgecolor','none')
TPnum = sum(gt(1,:)==2 & seizureProbability>noSeizureProbability);
end
if any(seizureProbability<noSeizureProbability)
bar(find(seizureProbability<noSeizureProbability),ones(1,nnz(seizureProbability<noSeizureProbability)),1,'red','edgecolor','none')
TNnum = sum(gt(1,:)==1 & seizureProbability<noSeizureProbability);
end
plot(seizureProbability,'k')
axis tight
hold off
accuracy5050 = (TPnum + TNnum)/Ntest;
title(['Test result for event "seizure", accuracy at 50/50: ' num2str(accuracy5050)])
% saveas(timePlot, [netDir filesep num2str(patientNames(patientInd)) '_wdwSz' num2str(wdwSz) '_timeseriesPlot_szOnly'], 'png');
% saveas(timePlot, [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_timeseriesPlot_szOnly'], 'png');