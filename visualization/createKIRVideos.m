%% generate KIR videos

% load directory list
patName = 'IM1198-2';
patientDir = ['G:\KinectV2' filesep patName filesep 'Kinect'];
eventDir = dir(patientDir);

% create one video per frame
% for eventInd = 3:numel(eventDir)
for eventInd = 11 % 3:numel(eventDir)
    if isdir([patientDir filesep eventDir(eventInd).name])
        kirFiles = dir([patientDir filesep eventDir(eventInd).name filesep '*.kir']);
        if isempty(kirFiles)
            warning(['No KIR files found in ' patientDir filesep eventDir(eventInd).name])
            continue
        end
        irVideo             = VideoWriter([patName eventDir(eventInd).name '_irVideo'],'MPEG-4');%'Grayscale AVI');
        irVideo.Quality     = 90; 
        irVideo.FrameRate   = 30;
        open(irVideo)
        for kirInd = 1:numel(kirFiles)
            [irContainer, timestamps] = playKIR([patientDir filesep eventDir(eventInd).name filesep kirFiles(kirInd).name]);
            N=numel(timestamps);
            for frameInd = 1:N
                im = irContainer(:,(256-212+1):(256+212),frameInd);
                if KinectRotate
                    im=rot90(im);
                end
                indexedImg = single(histeq(im, 1000))/(2^16-1);
                indexedImg = insertText(indexedImg,[1 1; 1 30; 1 60],{num2str(timestamps(frameInd),17) ['Frame ' num2str(frameInd)] ['Video ' num2str(kirInd)]});
                writeVideo(irVideo,indexedImg);
            end
        end
        close(irVideo)
    end
end
