function [net, info] = cnn_szrdet(imdb, varargin)
% cnn_szrdet

opts.dataDir = fullfile('C:\Projects\SeizureDetection\data\networks\') ;
opts.expDir = opts.dataDir ;
opts.imdbPath = fullfile(opts.expDir, 'dummy.mat');
opts.train.batchSize = 10 ;
opts.train.numEpochs = 50 ;
opts.train.continue = true ;
opts.train.gpus = 1;
% opts.train.learningRate = [(0.0001)+(0.001-0.0001)*exp(-(0:30))/(1-exp(-30)) 0.0001*ones(1,20)] ;
opts.train.learningRate = 0.001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
opts.train.plotDiagnostics = false ;

opts.train.numSubBatches = 1 ;

opts.networkType = 1;
opts.train.windowSize = 1;

% opts.train.use15FPSpredecessor = false;
% opts.train.use15FramesWindow  = false;
% opts.train.use10FramesWindow = false ;
% opts.train.use5FramesWindow = true ;

opts.train.pretrainNet = '';

opts = vl_argparse(opts, varargin) ;

nRow = size(imdb.images.data,1);
nCol = size(imdb.images.data,2);

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------
if ~isstruct(imdb)
    error('Test and train set, they are not defined!!! #bug')
disp('loading IMDB...')
load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');
diffSmpl = numel(imdb.meta.patientIDvec)-numel(imdb.meta.valid150Frames);
imdb.meta.patientIDvec = imdb.meta.patientIDvec(1:(end-diffSmpl/2));
disp('IMBD is in memory!')
% % use depth only
% imdb.images.data = imdb.images.data(:,:,2,:);
% % use IR only
% imdb.images.data = imdb.images.data(:,:,1,:);
end


% --------------------------------------------------------------------
%                                                   Network definition
% --------------------------------------------------------------------

switch opts.networkType
    case 1
        networkDefinition_NoLCN_singleOutput_SLIM
    case 2
        networkDefinition_NoLCN_singleOutput_SLIM_SHORT
    case 3
        networkDefinition_NoLCN_singleOutput_SLIMMER_SHORT
    case 4
        networkDefinition_NoLCN_singleOutput_SLIMMER_SHORTER
    case 5
        networkDefinition_NoLCN_singleOutput_ONELAYER
    case 6
        networkDefinition_NoLCN_singleOutput_SLIMMER_LONGER
    otherwise
        error('something''s wrong')
end

% networkDefinition_NoLCN_singleOutput_VGG_F_preTrain_diffNet
% networkDefinition_NoLCN_allReLu_dropoutAtBegining_singleOutput
% networkDef_standard_200resInput
% networkDefinition_standard
% networkDefinition_standard_singleOutput
% networkDefinition_NoLCN_singleOutput
% networkDefinition_NoLCN_DualOutput
% networkDefinition_4ChannelsToFully
% networkDefinition_4ChannelsToFully_4FCLayers
% networkDefinition_8ChannelsToFully_4FCLayers
% networkDefinition_1ChannelToFully_4FCLayers

if ~strcmp(opts.train.pretrainNet,'')
    load(opts.train.pretrainNet)
end
% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

%%%
% Pixelweise mean/sdev
%%%
% Take the mean out
meanImage = mean(imdb.images.data(:,:,:,imdb.images.set==1),4);
% meanImage = zeros(100,100,2,'single');
imdb.images.data = bsxfun(@minus, imdb.images.data, meanImage) ;
net.meta.meanImage = meanImage;
% divide by the standard variation [factor (1/(n-1))]
% stdDevImage = std(imdb.images.data(:,:,:,boolean(imdb.meta.valid10Frames) & imdb.images.set==1),0,4);
stdDevImage = ones(100,100,1,'single');
% imdb.images.data = bsxfun(@rdivide, imdb.images.data, stdDevImage) ;
net.meta.stdDevImage = stdDevImage;

%%%
% Convolutional mean/sdev
%%%
% % Take the mean out
% meanImageIR = mean(reshape(imdb.images.data(:,:,1,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,1,:) = bsxfun(@minus, imdb.images.data(:,:,1,:), meanImageIR) ;
% meanImageDepth = mean(reshape(imdb.images.data(:,:,2,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,2,:) = bsxfun(@minus, imdb.images.data(:,:,2,:), meanImageDepth) ;
% net.meta.meanImage = [meanImageIR meanImageDepth];
% % divide by the standard variation [factor (1/(n-1))]
% stdDevImageIR = std(reshape(imdb.images.data(:,:,1,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,1,:) = bsxfun(@rdivide, imdb.images.data(:,:,1,:), stdDevImageIR) ;
% stdDevImageDepth = std(reshape(imdb.images.data(:,:,2,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,2,:) = bsxfun(@rdivide, imdb.images.data(:,:,2,:), stdDevImageDepth) ;
% net.meta.stdDevImage = [stdDevImageIR stdDevImageDepth];

% if opts.train.use15FPSpredecessor
% [net, info] = cnn_train_balance(net, imdb, @getTemporalBatch, ...
%     opts.train) ;    
% elseif opts.train.use10FramesWindow
% [net, info] = cnn_train_balance(net, imdb, @get10FramesBatch, ...
%     opts.train) ;
% elseif opts.train.use5FramesWindow
% [net, info] = cnn_train_balance(net, imdb, @get5FramesBatch, ...
%     opts.train) ;
% elseif opts.train.use15FramesWindow
% [net, info] = cnn_train_balance(net, imdb, @get15FramesBatch, ...
%     opts.train) ;
% else
% [net, info] = cnn_train_balance(net, imdb, @getBatch, ...
%     opts.train) ;
% end
[net, info] = cnn_train_balance(net, imdb, @(x,y,z)getBatch(x,y,z,opts.train.windowSize,nRow,nCol), opts.train) ;

end
% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch, boolFlip, wdwSize, nRow, nCol)
% --------------------------------------------------------------------
if isempty(batch) || any(batch == 0)
    im = [];
    labels = [];
else
    batchSize = numel(batch);
    if batchSize > 1
    stIndices = interp2([batch(:)-wdwSize+1,batch(:)],linspace(1,2,wdwSize),(1:batchSize)')';
    else
    stIndices = interp1([batch(:)-wdwSize+1,batch(:)],linspace(1,2,wdwSize))';
    end
%     try
    im = reshape(imdb.images.data(:,:,:,uint32(stIndices(:))),nRow,nCol,wdwSize,[]) ;
%     catch
%     error('what')    
%     end
    if boolFlip
    im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
    end
    labels = imdb.images.labels(:,:,1,batch) ;
end

end

% function [im, labels] = getTemporalBatch(imdb,batch,boolFlip)
% if isempty(batch) || any(batch == 0)
%     im = [];
%     labels = [];
% else
% %     im = imresize(cat(3, imdb.images.data(:,:,:,batch-1), imdb.images.data(:,:,:,batch)),[224 224]) ;
%     im = cat(3, imdb.images.data(:,:,:,batch-1), imdb.images.data(:,:,:,batch)) ;
%     if boolFlip
%     im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
%     end
%     labels = imdb.images.labels(:,:,1,batch) ;
% end
% end
% 
% function [im, labels] = get5FramesBatch(imdb,batch,boolFlip)
% if isempty(batch) || any(batch == 0)
%     im = [];
%     labels = [];
% else
%     im = cat(3, imdb.images.data(:,:,:,batch-4),imdb.images.data(:,:,:,batch-3),imdb.images.data(:,:,:,batch-2),imdb.images.data(:,:,:,batch-1), imdb.images.data(:,:,:,batch)) ;
%     if boolFlip
%     im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
%     end
%     labels = imdb.images.labels(:,:,1,batch) ;
% end
% end
% 
% function [im, labels] = get10FramesBatch(imdb,batch,boolFlip)
% if isempty(batch) || any(batch == 0)
%     im = [];
%     labels = [];
% else
%     im = cat(3, imdb.images.data(:,:,:,batch-9),imdb.images.data(:,:,:,batch-8),imdb.images.data(:,:,:,batch-7),imdb.images.data(:,:,:,batch-6),imdb.images.data(:,:,:,batch-5),imdb.images.data(:,:,:,batch-4),imdb.images.data(:,:,:,batch-3),imdb.images.data(:,:,:,batch-2),imdb.images.data(:,:,:,batch-1), imdb.images.data(:,:,:,batch)) ;
%     if boolFlip
%     im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
%     end
%     labels = imdb.images.labels(:,:,1,batch) ;
% end
% end
% 
% function [im, labels] = get15FramesBatch(imdb,batch,boolFlip)
% if isempty(batch) || any(batch == 0)
%     im = [];
%     labels = [];
% else
%     im = cat(3, imdb.images.data(:,:,:,batch-14),imdb.images.data(:,:,:,batch-13),imdb.images.data(:,:,:,batch-12),imdb.images.data(:,:,:,batch-11),imdb.images.data(:,:,:,batch-10),imdb.images.data(:,:,:,batch-9),imdb.images.data(:,:,:,batch-8),imdb.images.data(:,:,:,batch-7),imdb.images.data(:,:,:,batch-6),imdb.images.data(:,:,:,batch-5),imdb.images.data(:,:,:,batch-4),imdb.images.data(:,:,:,batch-3),imdb.images.data(:,:,:,batch-2),imdb.images.data(:,:,:,batch-1), imdb.images.data(:,:,:,batch)) ;
%     if boolFlip
%     im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
%     end
%     labels = imdb.images.labels(:,:,1,batch) ;
% end
% end
