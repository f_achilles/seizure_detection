function [nets, info] = cnn_szrdet_multiFocus(imdb, varargin)
% cnn_szrdet

opts.dataDir = fullfile('C:\Projects\SeizureDetection\data\networks\') ;
opts.expDir = opts.dataDir ;
opts.imdbPath = fullfile(opts.expDir, 'dummy.mat');
opts.train.batchSize = 10 ;
opts.train.numEpochs = 50 ;
opts.train.continue = true ;
opts.train.gpus = 1;
% opts.train.learningRate = [(0.0001)+(0.001-0.0001)*exp(-(0:30))/(1-exp(-30)) 0.0001*ones(1,20)] ;
opts.train.learningRate = 0.001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
opts.train.plotDiagnostics = false ;

opts.train.numSubBatches = 1 ;

opts.networkType = 1;
opts.train.windowSize = 1;

opts.train.pretrainNet = '';

opts = vl_argparse(opts, varargin) ;

nRow = size(imdb.images.data,1);
nCol = size(imdb.images.data,2);

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------
if ~isstruct(imdb)
    error('Test and train set, they are not defined!!! #bug')
disp('loading IMDB...')
load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');
diffSmpl = numel(imdb.meta.patientIDvec)-numel(imdb.meta.valid150Frames);
imdb.meta.patientIDvec = imdb.meta.patientIDvec(1:(end-diffSmpl/2));
disp('IMBD is in memory!')
end


% --------------------------------------------------------------------
%                                                   Network definition
% --------------------------------------------------------------------

switch opts.networkType
    case 1
        networkDefinition_NoLCN_singleOutput_SLIM
    case 2
        networkDefinition_NoLCN_singleOutput_SLIM_SHORT
    case 3
        networkDefinition_NoLCN_singleOutput_SLIMMER_SHORT
    case 4
        networkDefinition_NoLCN_singleOutput_SLIMMER_SHORTER
    case 5
        networkDefinition_NoLCN_singleOutput_ONELAYER
    case 6
        networkDefinition_NoLCN_singleOutput_SLIMMER_LONGER
    case 7
        networkDefinition_NoLCN_singleOutput_SLIMMER_SHORTER_multiFocus
    otherwise
        error('something''s wrong')
end

% store net for global output and salient patch voting
nets.globalNet = net;


% networkDefinition_NoLCN_singleOutput_VGG_F_preTrain_diffNet
% networkDefinition_NoLCN_allReLu_dropoutAtBegining_singleOutput
% networkDef_standard_200resInput
% networkDefinition_standard
% networkDefinition_standard_singleOutput
% networkDefinition_NoLCN_singleOutput
% networkDefinition_NoLCN_DualOutput
% networkDefinition_4ChannelsToFully
% networkDefinition_4ChannelsToFully_4FCLayers
% networkDefinition_8ChannelsToFully_4FCLayers
% networkDefinition_1ChannelToFully_4FCLayers

if ~strcmp(opts.train.pretrainNet,'')
    load(opts.train.pretrainNet)
end
% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

%%%
% Pixelweise mean/sdev
%%%
% Take the mean out
% meanImage = mean(imdb.images.data(:,:,:,imdb.images.set==1),4);
meanImage = zeros(106,106,2,'single');
% imdb.images.data = bsxfun(@minus, imdb.images.data, meanImage) ;
net.meta.meanImage = meanImage;
% divide by the standard variation [factor (1/(n-1))]
% stdDevImage = std(imdb.images.data(:,:,:,boolean(imdb.meta.valid10Frames) & imdb.images.set==1),0,4);
stdDevImage = ones(106,106,1,'single');
% imdb.images.data = bsxfun(@rdivide, imdb.images.data, stdDevImage) ;
net.meta.stdDevImage = stdDevImage;

% init local patch net
networkDefinition_patchNet
nets.patchNet = net;
nets.patchNet.layers = cat(2,nets.globalNet.layers(1:4),nets.patchNet.layers);

[nets, info] = cnn_train_balance_multiFocus(nets, imdb, @(x,y,z)getBatch(x,y,z,opts.train.windowSize,nRow,nCol), opts.train) ;

end
% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch, boolFlip, wdwSize, nRow, nCol)
% --------------------------------------------------------------------
if isempty(batch) || any(batch == 0)
    im = [];
    labels = [];
else
    batchSize = numel(batch);
    if batchSize > 1
        stIndices = interp2([batch(:)-wdwSize+1,batch(:)],linspace(1,2,wdwSize),(1:batchSize)')';
    else
        stIndices = interp1([batch(:)-wdwSize+1,batch(:)],linspace(1,2,wdwSize))';
    end
%     im = reshape(imdb.images.data(:,:,:,uint32(stIndices(:))),nRow,nCol,wdwSize,[]) ;
%     % just the downscaled global image
%     im = reshape(imdb.images.data(:,:,1,uint32(stIndices(:))),nRow,nCol,wdwSize,[]) ;
    im = reshape(imdb.images.data(:,:,:,uint32(stIndices(:))),nRow,nCol,wdwSize*17,[]) ;

    if boolFlip
        im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
    end
    labels = imdb.images.labels(:,:,:,batch) ;
%     % just repeat the label
%     labels = repmat(imdb.images.labels(:,:,1,batch),1,17,1,1) ;

end

end

