function [im, labels] = getBatch_f(imdb, batch)
% --------------------------------------------------------------------
if isempty(batch) || any(batch == 0)
    im = [];
    labels = [];
else
    im = imdb.images.data(:,:,:,batch) ;
    labels = imdb.images.labels(:,:,:,batch) ;
end
end