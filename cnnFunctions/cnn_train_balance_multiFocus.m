function [nets, info] = cnn_train_balance_multiFocus(nets, imdb, getBatch, varargin)
% CNN_TRAIN   Demonstrates training a CNN
%    CNN_TRAIN() is an example learner implementing stochastic
%    gradient descent with momentum to train a CNN. It can be used
%    with different datasets and tasks by providing a suitable
%    getBatch function.
%
%    The function automatically restarts after each training epoch by
%    checkpointing.
%
%    The function supports training on CPU or on one or more GPUs
%    (specify the list of GPU IDs in the `gpus` option). Multi-GPU
%    support is relatively primitive but sufficient to obtain a
%    noticable speedup.

% Copyright (C) 2014-15 Andrea Vedaldi.
% All rights reserved.
%
% This file is part of the VLFeat library and is made available under
% the terms of the BSD license (see the COPYING file).

opts.batchSize = 256 ;
opts.numSubBatches = 1 ;
opts.train = [] ;
opts.val = [] ;
opts.numEpochs = 300 ;
opts.gpus = [] ; % which GPU devices to use (none, one, or more)
opts.learningRate = 0.001 ;
opts.continue = false ;
opts.expDir = fullfile('data','exp') ;
opts.conserveMemory = false ;
opts.backPropDepth = +inf ;
opts.sync = false ;
opts.prefetch = false ;
opts.weightDecay = 0.0005 ;
opts.momentum = 0.9 ;
opts.errorFunction = 'multiclass' ;
opts.errorLabels = {} ;
opts.plotDiagnostics = false ;
opts.memoryMapFile = fullfile(tempdir, 'matconvnet.bin') ;

opts.pretrainNet = '';
opts.windowSize = 1;

opts = vl_argparse(opts, varargin) ;

% number of patches
opts.nP = 16;

if ~exist(opts.expDir, 'dir'), mkdir(opts.expDir) ; end

if isempty(opts.train), opts.train = find(imdb.images.set==1) ; end
if isempty(opts.val), opts.val = find(imdb.images.set==2) ; end
if isnan(opts.train), opts.train = [] ; end

% prepare vars for generating balanced training sets in each epoch
boolNeg = imdb.images.labels(1,:,:,opts.train)==1;
boolPos = imdb.images.labels(1,:,:,opts.train)==2;
indsNeg = find(boolNeg);
indsPos = find(boolPos);
numNeg = nnz(boolNeg); %count negatives
numPos = nnz(boolPos); %count positives
diffNum = abs(numNeg-numPos);

% -------------------------------------------------------------------------
%                                                    Network initialization
% -------------------------------------------------------------------------

evaluateMode = isempty(opts.train) ;


if ~evaluateMode
subNets = {'globalNet', 'patchNet'};
for s=subNets
s=char(s);
net = nets.(s);
  for i=1:numel(net.layers)
    if isfield(net.layers{i}, 'weights')
      J = numel(net.layers{i}.weights) ;
      for j=1:J
        net.layers{i}.momentum{j} = zeros(size(net.layers{i}.weights{j}), 'single') ;
      end
      if ~isfield(net.layers{i}, 'learningRate')
        net.layers{i}.learningRate = ones(1, J, 'single') ;
      end
      if ~isfield(net.layers{i}, 'weightDecay')
        net.layers{i}.weightDecay = ones(1, J, 'single') ;
      end
    end
    % Legacy code: will be removed
    if isfield(net.layers{i}, 'filters')
      net.layers{i}.momentum{1} = zeros(size(net.layers{i}.filters), 'single') ;
      net.layers{i}.momentum{2} = zeros(size(net.layers{i}.biases), 'single') ;
      if ~isfield(net.layers{i}, 'learningRate')
        net.layers{i}.learningRate = ones(1, 2, 'single') ;
      end
      if ~isfield(net.layers{i}, 'weightDecay')
        net.layers{i}.weightDecay = single([1 0]) ;
      end
    end
  end
nets.(s) = net;
end
end

% setup GPUs
numGpus = numel(opts.gpus) ;
if numGpus > 1
  if isempty(gcp('nocreate')),
    parpool('local',numGpus) ;
    spmd, gpuDevice(opts.gpus(labindex)), end
  end
elseif numGpus == 1
  gpuDevice(opts.gpus)
end
if exist(opts.memoryMapFile), delete(opts.memoryMapFile) ; end

% setup error calculation function
if isstr(opts.errorFunction)
  switch opts.errorFunction
    case 'none'
      opts.errorFunction = @error_none ;
    case 'multiclass'
      opts.errorFunction = @error_multiclass ;
%       if isempty(opts.errorLabels), opts.errorLabels = {'top1e', 'top5e'} ; end
        if isempty(opts.errorLabels), opts.errorLabels = {'errGlob', 'errPatch'} ; end
    case 'binary'
      opts.errorFunction = @error_binary ;
      if isempty(opts.errorLabels), opts.errorLabels = {'bine'} ; end
    otherwise
      error('Uknown error function ''%s''', opts.errorFunction) ;
  end
end

% -------------------------------------------------------------------------
%                                                        Train and validate
% -------------------------------------------------------------------------

for epoch=1:opts.numEpochs
  learningRate = opts.learningRate(min(epoch, numel(opts.learningRate))) ;

  % fast-forward to where we stopped
  modelPath = @(ep) fullfile(opts.expDir, sprintf('net-epoch-%d.mat', ep));
  modelFigPath = fullfile(opts.expDir, 'net-train.pdf') ;
  if opts.continue
    if exist(modelPath(epoch),'file') , continue ; end
    if epoch > 1
      fprintf('resuming by loading epoch %d\n', epoch-1) ;
      load(modelPath(epoch-1), 'nets', 'info') ;
%       sz = size(net.layers{1}.filters);
%       wdwSz = sz(3);
%       if wdwSz == opts.windowSize
%           % do nothing
%       elseif wdwSz == opts.windowSize-1
% %           newFilterChannel = gpuArray(0.01*randn(sz(1),sz(2),1,sz(4), 'single'));
%           net.layers{1}.filters = cat(3,net.layers{1}.filters(:,:,1,:),net.layers{1}.filters);
%           net.layers{1}.momentum{1} = cat(3,net.layers{1}.momentum{1}(:,:,1,:),net.layers{1}.momentum{1});
%           disp('First network filterbank was increased about one channel.')
%       else
%         error('loaded network does not match the current spatiotemporal window size(+1)!')
%       end

      % set the random number generator back to where we left off
%       rng(info.randomStatus);
      opts.continue = false;
    end    
  end
  
  % choose new fair training samples to reduce overfitting
  if numNeg > numPos
    randFairInds = randperm(numNeg,diffNum);
    boolNegFair = boolNeg;
    boolNegFair(indsNeg(randFairInds))=false;
    opts.trainFair = [opts.train(boolPos) opts.train(boolNegFair)];
  elseif numNeg < numPos
    randFairInds = randperm(numPos,diffNum);
    boolPosFair = boolPos;
    boolPosFair(indsPos(randFairInds))=false;
    opts.trainFair = [opts.train(boolPosFair) opts.train(boolNeg)];
  else
    % already equal!
  end
  train = opts.trainFair(randperm(numel(opts.trainFair))) ;
  val = opts.val ;

  % move CNN to GPU as needed
  if numGpus == 1
    nets.globalNet = vl_simplenn_move(nets.globalNet, 'gpu') ;
    nets.patchNet = vl_simplenn_move(nets.patchNet, 'gpu') ;
  elseif numGpus > 1
    spmd(numGpus)
      net_ = vl_simplenn_move(net, 'gpu') ;
    end
  end

  % train one epoch and validate
  if numGpus <= 1
    [nets,stats.train] = process_epoch(opts, @(x,y)getBatch(x,y,true), epoch, train, learningRate, imdb, nets) ;
    [~,stats.val] = process_epoch(opts, @(x,y)getBatch(x,y,false), epoch, val, 0, imdb, nets) ;
  else
    spmd(numGpus)
      [net_, stats_train_] = process_epoch(opts, getBatch, epoch, train, learningRate, imdb, net_) ;
      [~, stats_val_] = process_epoch(opts, getBatch, epoch, val, 0, imdb, net_) ;
    end
    stats.train = sum([stats_train_{:}],2) ;
    stats.val = sum([stats_val_{:}],2) ;
  end

  % save
  if evaluateMode, sets = {'val'} ; else sets = {'train', 'val'} ; end
  for f = sets
    f = char(f) ;
    n = numel(eval(f)) ;
    info.(f).speed(epoch) = n / stats.(f)(1) ;
    info.(f).objective(epoch) = stats.(f)(2) / n ;
    info.(f).error(:,epoch) = stats.(f)(3:end) / n ;
  end
  if numGpus > 1
    spmd(numGpus)
      net_ = vl_simplenn_move(net_, 'cpu') ;
    end
    net = net_{1} ;
  else
    nets.globalNet = vl_simplenn_move(nets.globalNet, 'cpu') ;
    nets.patchNet = vl_simplenn_move(nets.patchNet, 'cpu') ;
  end
  if ~evaluateMode, save(modelPath(epoch), 'nets', 'info') ; end

%   figure(1) ; clf ;
%   hasError = isa(opts.errorFunction, 'function_handle') ;
%   subplot(1,1+hasError,1) ;
%   if ~evaluateMode
%     semilogy(1:epoch, info.train.objective, '.-', 'linewidth', 2) ;
%     hold on ;
%   end
%   semilogy(1:epoch, info.val.objective, '.--') ;
%   xlabel('training epoch') ; ylabel('energy') ;
%   grid on ;
%   h=legend(sets) ;
%   set(h,'color','none');
%   title('objective') ;
%   if hasError
%     subplot(1,2,2) ; leg = {} ;
%     if ~evaluateMode
%       plot(1:epoch, info.train.error', '.-', 'linewidth', 2) ;
%       hold on ;
%       leg = horzcat(leg, strcat('train ', opts.errorLabels)) ;
%     end
%     plot(1:epoch, info.val.error', '.--') ;
%     leg = horzcat(leg, strcat('val ', opts.errorLabels)) ;
%     set(legend(leg{:}),'color','none') ;
%     grid on ;
%     xlabel('training epoch') ; ylabel('error') ;
%     title('error') ;
%   end
%   drawnow ;
%   print(1, modelFigPath, '-dpdf') ;
end

% -------------------------------------------------------------------------
function err = error_multiclass(opts, labels, res)
% -------------------------------------------------------------------------
% err(1,1) =  sum(double(gather(res(end).x))) ;
% err(2,1) =  sum(double(gather(res(end).x))) ;

predictions = gather(res(end-1).x) ;

if size(predictions,3)>2
predictions = reshape(predictions,[1,17,2,size(predictions,4)]);
[~,predictions] = max(predictions,[],3);
err(1,1) = nnz(predictions~=labels)/17;
else
[~,predictions] = max(predictions,[],3);
err(1,1) = nnz(predictions~=labels);
end
err(2,1) = err(1,1);
% predictions = gather(res(end-1).x) ;
% [~,predictions] = sort(predictions, 3, 'descend') ;
% error = ~bsxfun(@eq, predictions, reshape(labels, 1, 1, 1, [])) ;
% err(1,1) = sum(sum(sum(error(:,:,1,:)))) ;
% err(2,1) = sum(sum(sum(min(error(:,:,1:5,:),[],3)))) ;

% -------------------------------------------------------------------------
function err = error_binaryclass(opts, labels, res)
% -------------------------------------------------------------------------
predictions = gather(res(end-1).x) ;
error = bsxfun(@times, predictions, labels) < 0 ;
err = sum(error(:)) ;

% -------------------------------------------------------------------------
function err = error_none(opts, labels, res)
% -------------------------------------------------------------------------
err = zeros(0,1) ;

% -------------------------------------------------------------------------
function  [nets,stats,prof] = process_epoch(opts, getBatch, epoch, subset, learningRate, imdb, nets)
% -------------------------------------------------------------------------

% validation mode if learning rate is zero
training = learningRate > 0 ;
if training, mode = 'training' ; else, mode = 'validation' ; end
if nargout > 2, mpiprofile on ; end

numGpus = numel(opts.gpus) ;
if numGpus >= 1
  one = gpuArray(single(1)) ;
else
  one = single(1) ;
end
res = cell(1,17) ;
mmap = [] ;
stats = [] ;

for t=1:opts.batchSize:numel(subset)
  fprintf('%s: epoch %02d: batch %3d/%3d: ', mode, epoch, ...
          fix(t/opts.batchSize)+1, ceil(numel(subset)/opts.batchSize)) ;
  batchSize = min(opts.batchSize, numel(subset) - t + 1) ;
  batchTime = tic ;
  numDone = 0 ;
  error = [] ;
  errorP = [] ;
%   nets.patchNet.layers = cat(2,nets.globalNet.layers(1:4),nets.patchNet.layers);
  for s=1:opts.numSubBatches
    % get this image batch and prefetch the next
    batchStart = t + (labindex-1) + (s-1) * numlabs ;
    batchEnd = min(t+opts.batchSize-1, numel(subset)) ;
    batch = subset(batchStart : opts.numSubBatches * numlabs : batchEnd) ;
    [im, labels] = getBatch(imdb, batch) ;
    % shold solve a bug when incorrect numbering of the batches leads to an
    % empty batch
    if isempty(im)
        continue
    end
    if opts.prefetch
      if s==opts.numSubBatches
        batchStart = t + (labindex-1) + opts.batchSize ;
        batchEnd = min(t+2*opts.batchSize-1, numel(subset)) ;
      else
        batchStart = batchStart + numlabs ;
      end
      nextBatch = subset(batchStart : opts.numSubBatches * numlabs : batchEnd) ;
      getBatch(imdb, nextBatch) ;
    end

    if numGpus >= 1
      im = gpuArray(im) ;
    end

    % evaluate CNN
    if training, dzdy = one; else, dzdy = [] ; end
    
    % local net
    nets.patchNet.layers{end}.class = labels ;
    labelCollector = zeros(1,opts.nP,1,numel(batch),'single');
    for iPatch = 1:opts.nP
    res{iPatch} = vl_simplenn_f(nets.patchNet, im(:,:,(iPatch+1):(opts.nP+1):end,:), dzdy, res{iPatch}, ...
                      'accumulate', s ~= 1, ...
                      'disableDropout', ~training, ...
                      'conserveMemory', opts.conserveMemory, ...
                      'backPropDepth', opts.backPropDepth, ...
                      'sync', opts.sync) ;
    % accumulate training errors
    errorP = sum([errorP, [...
      sum(double(gather(res{iPatch}(end).x))) ;
      reshape(opts.errorFunction(opts, labels, res{iPatch}),[],1) ; ]],2) ;
  
    [~,predictions] = max(gather(res{iPatch}(end-1).x),[],3);
    labelCollector(:,iPatch,:,:) = 1+(predictions==labels);
    end
    
    % global net
    % 1) generate labels
    labels = cat(2,labels,labelCollector) ;
    nets.globalNet.layers{end}.class = labels;
        
    % 2) perform simplenn
    res{opts.nP+1} = vl_simplenn_f(nets.globalNet, im(:,:,1:(opts.nP+1):end,:), dzdy, res{opts.nP+1}, ...
                      'accumulate', s ~= 1, ...
                      'disableDropout', ~training, ...
                      'conserveMemory', opts.conserveMemory, ...
                      'backPropDepth', opts.backPropDepth, ...
                      'sync', opts.sync) ;
    
    % accumulate training errors
    error = sum([error, [...
      sum(double(gather(res{opts.nP+1}(end).x))) ;
      reshape(opts.errorFunction(opts, labels, res{opts.nP+1}),[],1) ; ]],2) ;
  
    numDone = numDone + numel(batch) ;
  end
  errorP = errorP/opts.nP;
  
  % gather and accumulate gradients across labs
  if training
    if numGpus <= 1
      % accumulate and add gradients for patchNet (in res{1})
      for l=1:numel(nets.patchNet.layers)
          for j=1:numel(res{1}(l).dzdw)
              res{1}(l).dzdw{j} = res{1}(l).dzdw{j}/opts.nP;
          end
      end
      for iPatch = 2:opts.nP
        for l=1:numel(nets.patchNet.layers)
            for j=1:numel(res{iPatch}(l).dzdw)
              res{1}(l).dzdw{j} = res{1}(l).dzdw{j}+res{iPatch}(l).dzdw{j}/opts.nP;
            end
        end
      end
      nets.patchNet = accumulate_gradients(opts, learningRate, batchSize, nets.patchNet, res{1}) ;
      
      % accumulate and add gradients for globalNet
%       for j=1:numel(res{opts.nP+1}(1).dzdw)
%         res{opts.nP+1}(1).dzdw{j} = res{opts.nP+1}(1).dzdw{j} + res{1}(1).dzdw{j} / opts.nP;
%       end
      nets.globalNet = accumulate_gradients(opts, learningRate, batchSize, nets.globalNet, res{opts.nP+1}) ;
    else
      if isempty(mmap)
        mmap = map_gradients(opts.memoryMapFile, net, res, numGpus) ;
      end
      write_gradients(mmap, net, res) ;
      labBarrier() ;
      [net,res] = accumulate_gradients(opts, learningRate, batchSize, net, res, mmap) ;
    end
  end
  
%   % split patchNet from globalNet
%   nets.patchNet.layers = nets.patchNet.layers(5:end);
  
  % print learning statistics
  batchTime = toc(batchTime) ;
  stats = sum([stats,[batchTime ; error(1,:); errorP(1,:)]],2); % works even when stats=[]
  speed = batchSize/batchTime ;

  fprintf(' %.2f s (%.1f data/s)', batchTime, speed) ;
  n = (t + batchSize - 1) / max(1,numlabs) ;
  fprintf(' obj:%.3g', stats(2)/n) ;
  for i=1:numel(opts.errorLabels)
    fprintf(' %s:%.3g', opts.errorLabels{i}, stats(i+1)/n) ;
  end
  fprintf(' [%d/%d]', numDone, batchSize);
  fprintf('\n') ;

  % debug info
  if opts.plotDiagnostics && numGpus <= 1
    figure(2) ; vl_simplenn_diagnose(net,res) ; drawnow ;
  end
end

if nargout > 2
  prof = mpiprofile('info');
  mpiprofile off ;
end

% -------------------------------------------------------------------------
function [net,res] = accumulate_gradients(opts, lr, batchSize, net, res, mmap)
% -------------------------------------------------------------------------
for l=1:numel(net.layers)
  for j=1:numel(res(l).dzdw)
    thisDecay = opts.weightDecay * net.layers{l}.weightDecay(j) ;
    thisLR = lr * net.layers{l}.learningRate(j) ;

    % accumualte from multiple labs (GPUs) if needed
    if nargin >= 6
      tag = sprintf('l%d_%d',l,j) ;
      tmp = zeros(size(mmap.Data(labindex).(tag)), 'single') ;
      for g = setdiff(1:numel(mmap.Data), labindex)
        tmp = tmp + mmap.Data(g).(tag) ;
      end
      res(l).dzdw{j} = res(l).dzdw{j} + tmp ;
    end

    if isfield(net.layers{l}, 'weights')
      net.layers{l}.momentum{j} = ...
        single(opts.momentum) * net.layers{l}.momentum{j} ...
        - thisDecay * net.layers{l}.weights{j} ...
        - single(1/batchSize) * res(l).dzdw{j} ;
      net.layers{l}.weights{j} = net.layers{l}.weights{j} + thisLR * net.layers{l}.momentum{j} ;
    else
      % Legacy code: to be removed
      if j == 1
        net.layers{l}.momentum{j} = ...
          opts.momentum * net.layers{l}.momentum{j} ...
          - thisDecay * net.layers{l}.filters ...
          - (1 / batchSize) * res(l).dzdw{j} ;
        net.layers{l}.filters = net.layers{l}.filters + thisLR * net.layers{l}.momentum{j} ;
      else
        net.layers{l}.momentum{j} = ...
          opts.momentum * net.layers{l}.momentum{j} ...
          - thisDecay * net.layers{l}.biases ...
          - (1 / batchSize) * res(l).dzdw{j} ;
        net.layers{l}.biases = net.layers{l}.biases + thisLR * net.layers{l}.momentum{j} ;
      end
    end
  end
end

% -------------------------------------------------------------------------
function mmap = map_gradients(fname, net, res, numGpus)
% -------------------------------------------------------------------------
format = {} ;
for i=1:numel(net.layers)
  for j=1:numel(res(i).dzdw)
    format(end+1,1:3) = {'single', size(res(i).dzdw{j}), sprintf('l%d_%d',i,j)} ;
  end
end
format(end+1,1:3) = {'double', [3 1], 'errors'} ;
if ~exist(fname) && (labindex == 1)
  f = fopen(fname,'wb') ;
  for g=1:numGpus
    for i=1:size(format,1)
      fwrite(f,zeros(format{i,2},format{i,1}),format{i,1}) ;
    end
  end
  fclose(f) ;
end
labBarrier() ;
mmap = memmapfile(fname, 'Format', format, 'Repeat', numGpus, 'Writable', true) ;

% -------------------------------------------------------------------------
function write_gradients(mmap, net, res)
% -------------------------------------------------------------------------
for i=1:numel(net.layers)
  for j=1:numel(res(i).dzdw)
    mmap.Data(labindex).(sprintf('l%d_%d',i,j)) = gather(res(i).dzdw{j}) ;
  end
end
