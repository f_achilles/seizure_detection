% Define a network similar to Krishzevski
f=1/100 ;

net.layers = {} ;
% input size 100x100
% normalize
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;
%%%
% block 1
%%%
if opts.use15FPSpredecessor
    net.layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(9,9,4,16, 'single'), ...
                               'biases', zeros(1, 16, 'single'), ...
                               'stride', 1, ...
                               'pad', 0) ;
elseif opts.use10FramesWindow
    net.layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(9,9,20,16, 'single'), ...
                               'biases', zeros(1, 16, 'single'), ...
                               'stride', 1, ...
                               'pad', 0) ;
else
    net.layers{end+1} = struct('type', 'conv', ...
                               'filters', f*randn(9,9,1,16, 'single'), ...
                               'biases', zeros(1, 16, 'single'), ...
                               'stride', 1, ...
                               'pad', 0) ;
end
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ; %46x46
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;
%%%
% block 2
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(7,7,16,4, 'single'),...
                           'biases', zeros(1,4,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;%40x40
net.layers{end+1} = struct('type', 'relu') ;
% net.layers{end+1} = struct('type', 'pool', ...
%                            'method', 'max', ...
%                            'pool', [2 2], ...
%                            'stride', 2, ...
%                            'pad', 0) ;
% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;
%%%
% block 3
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,4,2, 'single'),...
                           'biases', zeros(1,2,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;%36x36
net.layers{end+1} = struct('type', 'relu') ;
% net.layers{end+1} = struct('type', 'pool', ...
%                            'method', 'max', ...
%                            'pool', [2 2], ...
%                            'stride', 2, ...
%                            'pad', 0) ;
% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;
%%%
% fully connected layer 1
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(36,36,2,512, 'single'),...
                           'biases', zeros(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
% net.layers{end+1} = struct('type', 'dropout', ...
%                            'rate', 0.5) ;
%%%
% fully connected layer 2
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,512,512, 'single'),...
                           'biases', zeros(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
% net.layers{end+1} = struct('type', 'dropout', ...
%                            'rate', 0.5) ;
%%%
% fully connected layer 3
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,512,64, 'single'),...
                           'biases', zeros(1,64,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
%%%
% fully connected layer 4
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,64,4, 'single'),...
                           'biases', zeros(1,4,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
%%%
% reshape layer
%%%
net.layers{end+1} = struct('type', 'reshape', ...
                           'param', [4 2 1 2]) ;
%%%
% loss
%%%
net.layers{end+1} = struct('type', 'softmaxloss') ;