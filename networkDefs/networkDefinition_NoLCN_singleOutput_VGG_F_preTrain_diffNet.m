% Define a network based on VGG-F imagenet
f=1/100 ;

% load pretrained network
net = load('C:\Users\felix\Downloads\imagenet-vgg-f.mat');

% redefine first layer
% % Option 1: reset
% net.layers{1} = struct('type', 'conv', ...
%                    'weights', {{f*randn(11,11,1,64, 'single'),...
%                                 zeros(1, 64, 'single')}}, ...
%                    'stride', 1, ...
%                    'pad', 0) ;

% Option 2: surgery [take only the filters for 'red' channel]
net.layers{1}.weights{1} = net.layers{1}.weights{1}(:,:,1,:);
% duplicate filters and negate them
net.layers{1}.weights{1} = cat(3,-net.layers{1}.weights{1},net.layers{1}.weights{1});
% keep biases

% insert a dropout before 5th layer
net.layers{end+1}   = []; % dummy layer
net.layers(6:end)   = net.layers(5:(end-1)); % shift layers to back
net.layers{5}       = struct('type','dropout', 'rate', 0.5);

% reset layer 20
net.layers{end-1} = struct('type', 'conv', ...
                   'weights', {{f*randn(1,1,4096,2, 'single'),...
                                zeros(1, 2, 'single')}}, ...
                   'stride', 1, ...
                   'pad', 0) ;
%%%
% loss
%%%
net.layers{end} = struct('type', 'softmaxloss') ;