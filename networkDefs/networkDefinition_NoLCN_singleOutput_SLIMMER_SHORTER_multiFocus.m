% Define a network
f=1/100 ;

net.layers = {} ;
% input size 106x106

%%%
% normalize
%%%

% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;

%%%
% block 1
%%%

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,10,16, 'single'), ...
                           'biases', zeros(1, 16, 'single'), ...
                           'stride', 3, ...
                           'pad', 0) ;% 34 34
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;% 17 17
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;
%%%
% block 2
%%%
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;
% net.layers{end+1} = struct('type', 'conv', ...
%                            'filters', f*randn(3,3,8,8, 'single'),...
%                            'biases', zeros(1,8,'single'), ...
%                            'stride', 1, ...
%                            'pad', 0) ;% 30 30
% net.layers{end+1} = struct('type', 'relu') ;
% net.layers{end+1} = struct('type', 'pool', ...
%                            'method', 'max', ...
%                            'pool', [3 3], ...
%                            'stride', 3, ...
%                            'pad', 0) ;% 10 10
% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;
% % %%%
% % block 3
% %%%
% net.layers{end+1} = struct('type', 'conv', ...
%                            'filters', f*randn(5,5,16,16, 'single'),...
%                            'biases', zeros(1,16,'single'), ...
%                            'stride', 1, ...
%                            'pad', 0) ;
% net.layers{end+1} = struct('type', 'relu') ;
% net.layers{end+1} = struct('type', 'pool', ...
%                            'method', 'max', ...
%                            'pool', [2 2], ...
%                            'stride', 2, ...
%                            'pad', 0) ;
% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;
%%%
% fully connected layer 1
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(17,17,16,128, 'single'),...
                           'biases', zeros(1,128,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;
% %%%
% % fully connected layer 2
% %%%
% net.layers{end+1} = struct('type', 'conv', ...
%                            'filters', f*randn(1,1,256,256, 'single'),...
%                            'biases', zeros(1,256,'single'), ...
%                            'stride', 1, ...
%                            'pad', 0,...
%                            'filtersLearningRate', 1, ...
%                            'biasesLearningRate', 1) ;
% % net.layers{end+1} = struct('type', 'dropout', ...
% %                            'rate', 0.5) ;
%%%
% fully connected layer 3
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,128,2+16*2, 'single'),...
                           'biases', zeros(1,2+16*2,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
%%%
% loss
%%%
net.layers{end+1} = struct('type', 'softmaxloss') ;
