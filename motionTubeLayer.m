function [Y,param,weights,dzdf] = motionTubeLayer(inputMatrix,weights,param,dzdy)
persistent meanFree forwardResult
forward = false;
if nargin == 3
    forward = true;
end
backward = ~forward;
% input is a spatio-temporal volume (video), with dimensions NxMxT
% inputMatrix = randn(100,100,30,10,'single');
% inputMatrix = reshape(vidMatrix,[100 100 30 10]);
sz =[size(inputMatrix,1) size(inputMatrix,2) size(inputMatrix,3) size(inputMatrix,4)];

% param.P = 9;
% param.n = 64;
% param.T = 30;
% param.firstRun = true;
% step 0: smoothen the input images with a gaussian kernel

% % step 1: initialize the filters (if called for the first time)
% f = 0.2;
% offset = 0.5;
% if param.firstRun
%     % weights represent continuous x- and y- coordinate
%     weights = f*(rand(2,1,param.T,param.n,'single')-0.5)+offset;
%     param.firstRun = false;
% end

% step 2: from single-valued x-y-positions, derive discrete px-coordinates
% inside PxP-kernel. create [0,1]-kernels, divide by T to normalize energy.
% store vector of locations (sub2ind) inside the PxPxT volume which are not
% 0, and their offset from the center.
discrFilterBank = gpuArray(zeros(param.P,param.P,param.T,param.n,'single'));
% squash inbetween 0.5 and P.4999, P being the width/heigth of each
% filter
Xdiscrete = squeeze(round(max(0,min(1,weights(1,:,:,:)))*(param.P*0.99)+0.5));
Ydiscrete = squeeze(round(max(0,min(1,weights(2,:,:,:)))*(param.P*0.99)+0.5));
discrFilterBank(sub2ind([param.P,param.P,param.T,param.n],...
    Ydiscrete,Xdiscrete,repmat((1:param.T)',1,param.n),repmat(1:param.n,param.T,1))) = 1/param.T;

if forward
% step 3: use vl_nnconv to convolve discrete kernels and smoothed input
meanMap = vl_nnconv(inputMatrix,discrFilterBank,[],'Pad',floor(param.P/2));

% step 4: use the result of step 3 (which is the mean) and subtract it from
% each frame's corresponding pixels, using the indices-vector from step 2.
% now compute the square, sum, and divide by (T-1) for each pixel. Now use
% the formula 1/(1+var) in order to squash the result to a (0,1]-range.
shiftedMean = gpuArray(zeros(sz(1)+2*floor(param.P/2),sz(2)+2*floor(param.P/2),param.T,sz(4),param.n,'single'));
shiftedMean((floor(param.P/2)+1):(end-floor(param.P/2)),(floor(param.P/2)+1):(end-floor(param.P/2)),:,:,:) = ...
    repmat(permute(meanMap,[1 2 5 4 3]),1,1,param.T,1,1);

% inputMatrix = gpuArray(inputMatrix);
% discrFilterBank = gpuArray(discrFilterBank);

% tic
varianceMap = gpuArray(zeros(sz(1),sz(2),param.n,sz(4),'single'));
% varianceCell = cell(1);
for iFilter = 1:param.n
    tempShiftedMean = shiftedMean(:,:,:,:,iFilter);
    for iFrame = 1:param.T
%         shiftedMean(:,:,iFrame,:,iFilter) = circshift(shiftedMean(:,:,iFrame,:,iFilter),...
%             [Ydiscrete(iFrame,iFilter) Xdiscrete(iFrame,iFilter)]-floor(param.P/2)-1);
        
        tempShiftedMean(:,:,iFrame,:) = circshift(tempShiftedMean(:,:,iFrame,:),...
            [Ydiscrete(iFrame,iFilter) Xdiscrete(iFrame,iFilter)]-floor(param.P/2)-1);
    end
    meanFree{iFilter} = bsxfun(@minus,inputMatrix,tempShiftedMean((floor(param.P/2)+1):(end-floor(param.P/2)),(floor(param.P/2)+1):(end-floor(param.P/2)),:,:));
    meanFree{iFilter} = param.T*(meanFree{iFilter}.^2)/(max(1,param.T-1)); %variance, not meanFree, but same variable to save memory
    varianceMap(:,:,iFilter,:) = vl_nnconv(meanFree{iFilter},discrFilterBank(:,:,:,iFilter),[],'Pad',floor(param.P/2));
%     varianceCell{iFilter} = vl_nnconv(meanFree,discrFilterBank(:,:,:,iFilter),[],'Pad',floor(param.P/2));
end
% 15.8 seconds on CPU
% GPU:
% fastest: 4.0 seconds on GPU (Tesla K40c)
% (8.0 seconds with parfor in inner loop only(why?), 4.2 seconds with parfor in outer loop,
% 19 seconds when NOT copying parts of shiftedMean to tempShiftedMean)
% toc

% divide by variance, so to squash it inbetween (0,1]
Y = 1/(1+varianceMap);
forwardResult = Y;
clear tempShiftedMean shiftedMean meanMap varianceMap
end % end of forward
%%
% tic
if backward
% dzdy = single(1);
% step 5: gradient!
dYdVar = -(forwardResult.^2);
dzdVar = dzdy .* dYdVar;
% dzdVarMap = gpuArray(zeros(sz(1),sz(2),param.n,sz(4),'single'));

dInputdXdir = (circshift(inputMatrix,[0 -1]) - circshift(inputMatrix,[0 +1]))/2;
dInputdYdir = (circshift(inputMatrix,[-1 0]) - circshift(inputMatrix,[+1 0]))/2;

dInputdXdir(:,1,:,:) = dInputdXdir(:,2,:,:);
dInputdXdir(:,end,:,:) = dInputdXdir(:,end-1,:,:);
dInputdYdir(1,:,:,:) = dInputdYdir(2,:,:,:);
dInputdYdir(end,:,:,:) = dInputdYdir(end-1,:,:,:);

Y = 0;
dzdf = gpuArray(zeros(2,1,param.T,param.n,'single'));
for iFilter = 1:param.n
    % compute the derivative filter-wise
    
    % we need the old meanFree(s)! persistent variable? each meanFree has
    % 12MB, so with 64 filter we have 768MB stored on GPU
    dzdVarMap = vl_nnconv(meanFree{iFilter},discrFilterBank(:,:,:,iFilter),[],dzdVar(:,:,iFilter,:),'Pad',floor(param.P/2));
    
    dVarMapdMeanFree = 2*param.T*meanFree{iFilter}/(max(1,param.T-1));
    dzdMeanFree = dVarMapdMeanFree .* dzdVarMap; % this is 100,100,30,10
    
    %dMeanFreedMeanMap = ?
%     dzdMeanMap = dMeanFreedMeanMap .* dzdMeanFree; % this needs to be 100,100,1,10
    dzdMeanMap = -sum(dzdMeanFree,3); %NO IDEA if this is correct, but interestingly all derivatives are positive here, does this make sense?
    
    dzdInput = vl_nnconv(inputMatrix,discrFilterBank(:,:,:,iFilter),[],dzdMeanMap,'Pad',floor(param.P/2));
    
    dzdXdir = dzdInput .* dInputdXdir;
    dzdYdir = dzdInput .* dInputdYdir;
    
    % aggregate directional derivatives for each frame
    dzdXFilter = mean(mean(dzdXdir,1),2);
    dzdYFilter = mean(mean(dzdYdir,1),2); %values are in range 1e-5 (if input is randn, hence mean-free)
    
    % aggregate derivative wrt input
    Y = Y+dzdInput/param.n;
    
    % aggregate derivative over all input samples
    dzdf(:,1,:,iFilter) = [ mean(dzdXFilter,4);
                            mean(dzdYFilter,4)];
end
    
end
% toc
% 4.3 seconds using GPU arrays (Tesla K40c)

end