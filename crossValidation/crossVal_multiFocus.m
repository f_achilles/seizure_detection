% one modality only
for networkType = 7

for cLR = 10^(-3.25) %10^(-3.25)
% vary learning rate   

for fold = 2 %1:2
    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_ir_Type%d_LR%.2f_fold%d_multiFocusTest4',networkType,log(cLR)/log(10),fold);
    mkdir(netDir);
    opts.expDir = netDir;
    opts.train.expDir = netDir;
for wdwSz = 10 %[2 5 10]
    % append valid indices for a time window of X (wdwSz) frames (!)
    validXFrames = 0*imdb.meta.valid15FPSvec;
    n=imdb.meta.dbsizes;
    N=n(end);
    if wdwSz > 1
        for i = wdwSz:N
            if imdb.meta.valid15FPSvec((i-wdwSz+2):i)==ones(1,wdwSz-1);
                validXFrames(i)=1;
            end
        end
    else
        validXFrames = ones(size(validXFrames));
    end
    
%     trainSetInds = [];
%     trainPatIDs = patientsSets{fold};
%     for pat = 1:numel(trainPatIDs)
%         temp = find(imdb.meta.patientIDvec==trainPatIDs(pat));
%         trainSetInds = cat(2,trainSetInds,temp);
%     end
%     
%     imdb.images.set = 2*ones(1,N);
%     imdb.images.set(trainSetInds)=1;
    % discard frames in the train+val set that have no 15FPS-predecessor
    imdb.images.set(~boolean(validXFrames)) = 3;
    
    opts.train.windowSize           = wdwSz;
    opts.train.learningRate         = cLR ;
    opts.train.numEpochs            = 50*wdwSz ;
    opts.train.batchSize            = 250 ;
    opts.train.numSubBatches        = 10 ;
    opts.networkType                = networkType;

    % train network
    [net, info]=cnn_szrdet_multiFocus(imdb,opts);

    % perform testing
    testInds = find(imdb.images.set == 2);
    Ntest = numel(testInds);
    testIdxVec = 1:Ntest;
    testResults = cell(1,Ntest);
    net.layers{end}.type = 'softmax';
    net = vl_simplenn_move(net,'gpu');
    for i=testIdxVec
        spatioTemporalVolume = reshape(imdb.images.data(:,:,:,(testInds(i)-wdwSz+1):testInds(i)),numRow,numCol,wdwSz,1);
        spatioTemporalVolume = bsxfun(@rdivide,bsxfun(@minus,spatioTemporalVolume ,net.meta.meanImage),net.meta.stdDevImage);
        res = vl_simplenn(net, gpuArray(spatioTemporalVolume), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', true, ...
        'sync', false) ;
        softmaxOut = gather(res(end).x);
        testResults{i}=softmaxOut;
    end
    % calculate results
    testResults = [testResults{:}];
    seizureProbability = squeeze(testResults(1,:,2));
    noSeizureProbability = squeeze(testResults(1,:,1));
    gt = reshape(squeeze(imdb.images.labels(1,:,:,testInds)),1,[]);
    gt = gt(testIdxVec);
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    %%%
    % Draw PR-curve, ROC-curve, accuracy-curve and colored timeseries
    %%%
%     drawResultCurves
    % store results
    save([netDir filesep 'wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
    % clean up
%     close all
%     % store last network
%     movefile([netDir filesep 'net-epoch-' num2str(opts.train.numEpochs) '.mat'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_NETep' num2str(opts.train.numEpochs) '.mat']);
%     % store training PDF
%     movefile([netDir filesep 'net-train.pdf'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_trainingPlot.pdf']);
%     % delete older networks
%     delete([netDir filesep 'net-epoch-*']);
end %windowSize
end %fold
end %LR
end %networkType