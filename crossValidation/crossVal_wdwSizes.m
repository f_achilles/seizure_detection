%% Training script for 2fold cross-validation of the expanded dataset
clear all
% depth round
load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');
diffSmpl = numel(imdb.meta.patientIDvec)-numel(imdb.meta.valid150Frames);
imdb.meta.patientIDvec = imdb.meta.patientIDvec(1:(end-diffSmpl/2));
% use depth only
imdb.images.data = imdb.images.data(:,:,2,:);
% append valid indices for a time window of 5 frames (!)
valid15Frames = 0*imdb.meta.valid15FPSvec;
n=imdb.meta.dbsizes;
N=n(end);
for i = 15:N
if imdb.meta.valid15FPSvec((i-15+2):i)==ones(1,15-1);
    valid15Frames(i)=1;
end
end
% store the boolean vector that knows all the valid 10er-last frames
imdb.meta.valid15Frames = valid15Frames;
%%
patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};

% depth only
for networkType = 4

for cLR = 10^(-3.25)
% vary learning rate   

for fold = 1:2
for wdwSz = 15 %[2 5 10]

    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_depth_Type%d_LR%.2f_fold%d_wdw%d',networkType,log(cLR)/log(10),fold,wdwSz);
    mkdir(netDir);
    opts.expDir = netDir;
    opts.train.expDir = netDir;
    
    trainSetInds = [];
    trainPatIDs = patientsSets{fold};
    for pat = 1:numel(trainPatIDs)
        temp = find(imdb.meta.patientIDvec==trainPatIDs(pat));
        trainSetInds = cat(2,trainSetInds,temp);
    end
    
    imdb.images.set = 2*ones(1,N);
    imdb.images.set(trainSetInds)=1;
    % discard frames in the train+val set that have no 15FPS-predecessor
    imdb.images.set(~boolean(imdb.meta.valid15FPSvec)) = 3;
    if wdwSz > 2
    % discard non-5-predecessor-frames
%     imdb.images.set(~boolean(imdb.meta.valid5Frames)) = 3;
    end
    if wdwSz > 5
    % discard non-10-predecessor-frames
    imdb.images.set(~boolean(imdb.meta.valid10Frames)) = 3;
    end
    if wdwSz > 10
    % discard non-15-predecessor-frames
    imdb.images.set(~boolean(imdb.meta.valid15Frames)) = 3;
    end
    
    opts.train.use15FPSpredecessor  = wdwSz == 2;
    opts.train.use5FramesWindow     = wdwSz == 5 ;
    opts.train.use10FramesWindow    = wdwSz == 10;
    opts.train.use15FramesWindow    = wdwSz == 15;
    opts.train.learningRate         = cLR ;
    opts.train.numEpochs            = 150 ;
    opts.train.batchSize            = 250 ;
    opts.train.numSubBatches        = 10 ;
    opts.networkType                = networkType;

%     clear imdb

    % train network
    [net, info]=cnn_szrdet(imdb,opts);
%     % low memory option:
%     [net, info]=cnn_szrdet(1,opts);
%     % load again for testing and final accuracy values
%     load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');

    testInds = find(imdb.images.set == 2);
    Ntest = numel(testInds);
    testResults = cell(1,Ntest);
    net.layers{end}.type = 'softmax';
    net = vl_simplenn_move(net,'gpu');
    % perform testing
    im1=[];im2=[];im3=[];im4=[];im5=[];im6=[];im7=[];im8=[];im9=[];im10=[];im11=[];im12=[];im13=[];im14=[];im15=[];
    testIdxVec = 1:Ntest;
    for i=testIdxVec
        im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
        if wdwSz > 1
        im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
        end
        if wdwSz > 2
        im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
        im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
        im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 5
        im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
        im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
        im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
        im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
        im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 10
        im11 = (imdb.images.data(:,:,:,testInds(i)-10)-net.meta.meanImage)./net.meta.stdDevImage;
        im12 = (imdb.images.data(:,:,:,testInds(i)-11)-net.meta.meanImage)./net.meta.stdDevImage;
        im13 = (imdb.images.data(:,:,:,testInds(i)-12)-net.meta.meanImage)./net.meta.stdDevImage;
        im14 = (imdb.images.data(:,:,:,testInds(i)-13)-net.meta.meanImage)./net.meta.stdDevImage;
        im15 = (imdb.images.data(:,:,:,testInds(i)-14)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        res = vl_simplenn(net, gpuArray(cat(3,im15,im14,im13,im12,im11,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
        softmaxOut = gather(res(end).x);
        testResults{i}=softmaxOut;
    end
    % calculate results
    testResults = [testResults{:}];
    seizureProbability = squeeze(testResults(1,:,2));
    noSeizureProbability = squeeze(testResults(1,:,1));
    gt = reshape(squeeze(imdb.images.labels(1,:,:,testInds)),1,[]);
    gt = gt(testIdxVec);
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    %%%
    % Draw PR-curve, ROC-curve, accuracy-curve and colored timeseries
    %%%
%     drawResultCurves
    % store results
    save([netDir filesep 'wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
    % clean up
%     close all
%     % store last network
%     movefile([netDir filesep 'net-epoch-' num2str(opts.train.numEpochs) '.mat'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_NETep' num2str(opts.train.numEpochs) '.mat']);
%     % store training PDF
%     movefile([netDir filesep 'net-train.pdf'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_trainingPlot.pdf']);
%     % delete older networks
%     delete([netDir filesep 'net-epoch-*']);
end %windowSize
end %fold
end %LR
end %networkType

%% Training script for 2fold cross-validation of the expanded dataset
% infrared round
clear all
load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');
diffSmpl = numel(imdb.meta.patientIDvec)-numel(imdb.meta.valid150Frames);
imdb.meta.patientIDvec = imdb.meta.patientIDvec(1:(end-diffSmpl/2));
% % use IR only
imdb.images.data = imdb.images.data(:,:,1,:);
% append valid indices for a time window of 5 frames (!)
valid15Frames = 0*imdb.meta.valid15FPSvec;
n=imdb.meta.dbsizes;
N=n(end);
for i = 15:N
if imdb.meta.valid15FPSvec((i-15+2):i)==ones(1,15-1);
    valid15Frames(i)=1;
end
end
% store the boolean vector that knows all the valid 10er-last frames
imdb.meta.valid15Frames = valid15Frames;
%%
patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};

% depth only
for networkType = 4

for cLR = 10^(-3.25)
% vary learning rate   

for fold = 1:2
for wdwSz = 15 %[2 5 10]

    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type%d_LR%.2f_fold%d_wdw%d',networkType,log(cLR)/log(10),fold,wdwSz);
    mkdir(netDir);
    opts.expDir = netDir;
    opts.train.expDir = netDir;
    
    trainSetInds = [];
    trainPatIDs = patientsSets{fold};
    for pat = 1:numel(trainPatIDs)
        temp = find(imdb.meta.patientIDvec==trainPatIDs(pat));
        trainSetInds = cat(2,trainSetInds,temp);
    end
    n=imdb.meta.dbsizes;
    N=n(end);
    imdb.images.set = 2*ones(1,N);
    imdb.images.set(trainSetInds)=1;
    % discard frames in the train+val set that have no 15FPS-predecessor
    imdb.images.set(~boolean(imdb.meta.valid15FPSvec)) = 3;
    if wdwSz > 2
    % discard non-5-predecessor-frames
%     imdb.images.set(~boolean(imdb.meta.valid5Frames)) = 3;
    end
    if wdwSz > 5
    % discard non-10-predecessor-frames
    imdb.images.set(~boolean(imdb.meta.valid10Frames)) = 3;
    end
    if wdwSz > 10
    % discard non-15-predecessor-frames
    imdb.images.set(~boolean(imdb.meta.valid15Frames)) = 3;
    end
    
    opts.train.use15FPSpredecessor  = wdwSz == 2;
    opts.train.use5FramesWindow     = wdwSz == 5 ;
    opts.train.use10FramesWindow    = wdwSz == 10;
    opts.train.use15FramesWindow    = wdwSz == 15;
    opts.train.learningRate         = cLR ;
    opts.train.numEpochs            = 150 ;
    opts.train.batchSize            = 250 ;
    opts.train.numSubBatches        = 10 ;
    opts.networkType                = networkType;

%     clear imdb

    % train network
    [net, info]=cnn_szrdet(imdb,opts);
%     % low memory option:
%     [net, info]=cnn_szrdet(1,opts);
%     % load again for testing and final accuracy values
%     load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');

    testInds = find(imdb.images.set == 2);
    Ntest = numel(testInds);
    testResults = cell(1,Ntest);
    net.layers{end}.type = 'softmax';
    net = vl_simplenn_move(net,'gpu');
    % perform testing
    im1=[];im2=[];im3=[];im4=[];im5=[];im6=[];im7=[];im8=[];im9=[];im10=[];im11=[];im12=[];im13=[];im14=[];im15=[];
    testIdxVec = 1:Ntest;
    for i=testIdxVec
        im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
        if wdwSz > 1
        im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage(:,:,:);
        end
        if wdwSz > 2
        im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
        im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
        im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 5
        im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
        im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
        im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
        im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
        im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 10
        im11 = (imdb.images.data(:,:,:,testInds(i)-10)-net.meta.meanImage)./net.meta.stdDevImage;
        im12 = (imdb.images.data(:,:,:,testInds(i)-11)-net.meta.meanImage)./net.meta.stdDevImage;
        im13 = (imdb.images.data(:,:,:,testInds(i)-12)-net.meta.meanImage)./net.meta.stdDevImage;
        im14 = (imdb.images.data(:,:,:,testInds(i)-13)-net.meta.meanImage)./net.meta.stdDevImage;
        im15 = (imdb.images.data(:,:,:,testInds(i)-14)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        res = vl_simplenn(net, gpuArray(cat(3,im15,im14,im13,im12,im11,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
        softmaxOut = gather(res(end).x);
        testResults{i}=softmaxOut;
    end
    % calculate results
    testResults = [testResults{:}];
    seizureProbability = squeeze(testResults(1,:,2));
    noSeizureProbability = squeeze(testResults(1,:,1));
    gt = reshape(squeeze(imdb.images.labels(1,:,:,testInds)),1,[]);
    gt = gt(testIdxVec);
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    %%%
    % Draw PR-curve, ROC-curve, accuracy-curve and colored timeseries
    %%%
%     drawResultCurves
    % store results
    save([netDir filesep 'wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
    % clean up
%     close all
%     % store last network
%     movefile([netDir filesep 'net-epoch-' num2str(opts.train.numEpochs) '.mat'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_NETep' num2str(opts.train.numEpochs) '.mat']);
%     % store training PDF
%     movefile([netDir filesep 'net-train.pdf'],...
%         [netDir filesep 'Fold' num2str(fold) '_wdwSz' num2str(wdwSz) '_trainingPlot.pdf']);
%     % delete older networks
%     delete([netDir filesep 'net-epoch-*']);
end %windowSize
end %fold
end %LR
end %networkType