% training script for cross-validation

if size(imdb.images.labels,1) == 3
    imdb.images.labelsOld = imdb.images.labels;
    imdb.images.labels = imdb.images.labels(1:2,:,:,:);
    imdb.images.labels = permute(imdb.images.labels,[1 3 4 2]);
%     imdb.images.labels = imdb.images.labels+1;
    % training is getting unstable when not dividing by 65.000, then
    % cutting at 0.1 and then multiplying by 10
%     imdb.images.data = single(imdb.images.data)/(2^(16)-1);
%     imdb.images.data(imdb.images.data>0.1) = 0.1;
%     imdb.images.data = 10*imdb.images.data;
end

% tic %% takes 10 seconds only 0.0 (for 11.000 images)
% for i=1:N
%     imdb.images.data(:,:,:,i) = medfilt2(imdb.images.data(:,:,:,i));
% end
% toc
%% alteration of the labels
imdb.images.seizureLabels = imdb.images.labels(1,:,:,:);
imdb.images.careLabels = imdb.images.labels(2,:,:,:);
%%


% save('C:\Projects\SeizureDetection\data\seizureDetectionTrainingData.mat','imdb','-v7.3');

n=imdb.meta.dbsizes;
N=n(end);
% train in a leave-one-dataset-out manner
patIDs = unique(imdb.meta.patientIDvec);
% define train and test set
trainSetInds=[];
testSetInds=[];
trainSeizureIDnums = [1 2 3 6 10 11 12 15 16 17 22];
testSeizureIDnums = [4 5 7 8 9 13 14 18 19 20 21];
for iEvent = testSeizureIDnums
    testSetInds=[testSetInds (n(iEvent)+1):n(iEvent+1)];
end
for iEvent = trainSeizureIDnums
    trainSetInds=[trainSetInds (n(iEvent)+1):n(iEvent+1)];
end

% random splits do not work (!)
iEvents = [2 4 6 1 3 5];%randperm(numel(n)-1);
for iEvent = iEvents(1:3)
    trainSetInds=[trainSetInds (n(iEvent)+1):n(iEvent+1)];
end
for iEvent = iEvents(4:end)
    testSetInds=[testSetInds (n(iEvent)+1):n(iEvent+1)];
end

% crop the range to the 99.9th percentile in order to get rid of "outliers"
Y = prctile(reshape(imdb.images.data(:,:,1,trainSetInds),1,[]),99);
if Y>4500
    imdb.images.data(imdb.images.data > Y) = Y;
else
    error('percentile was not calculated correctly')
end

%%% altering the labels
% imdb.images.labels = imdb.images.seizureLabels;
imdb.images.labels = permute(imdb.images.labelsOld(1:2,:),[1 3 4 2]);
%%%
%%
imdb.images.set = 3*ones(1,N);
imdb.images.set(trainSetInds)=1;
imdb.images.set(testSetInds)=2;
netDir = 'C:\Projects\SeizureDetection\data\networks\IM11982';
mkdir(netDir);
opts.expDir = netDir;%['C:\Projects\SeizureDetection\data\networks\version7' filesep 'crossVal' num2str(iCrossval) 'ReTrain'];
opts.train.expDir = netDir;
opts.train.use15FPSpredecessor = true;
opts.train.use10FramesWindow = false;
opts.train.use5FramesWindow = false ;
opts.train.learningRate = 0.001 ;
opts.train.numEpochs = 150 ; % 50 were trained before, now add 50 more
opts.train.batchSize = 50 ;
[net, info]=cnn_szrdet(imdb,opts);
%%
    
for iCrossval=6%:6 % Patient number 6 (11982) has 22 events, so we start to train without him!
    imdb.images.set = ones(1,N);
    imdb.images.set(imdb.meta.patientIDvec==patIDs(iCrossval))=2;
%     rng('default')
%     trainInds = find(imdb.images.set == 1);
%     randomIndices = randperm(numel(trainInds));
%     valSize = floor(numel(randomIndices)/5);
%     imdb.images.set(trainInds(randomIndices(1:valSize))) = 2;
    
    mkdir('C:\Projects\SeizureDetection\data\networks\version7',['crossVal' num2str(iCrossval)]);
    opts.expDir = ['C:\Projects\SeizureDetection\data\networks\version7' filesep 'crossVal' num2str(iCrossval)];
    opts.use15FPSpredecessor = false;
    opts.use10FramesWindow = true;
    [net, info]=cnn_szrdet(imdb,opts);
end

%% adding a sequence of the unknown patient
patIDsOfEvents = imdb.meta.patientIDvec(n(2:end));
for iCrossval=6%:6
    imdb.images.set = 3*ones(1,N);
    netDir = ['C:\Projects\SeizureDetection\data\networks\version7' filesep 'crossVal' num2str(iCrossval)];
    file = dir([netDir filesep '*-50.mat']);
%     load([netDir filesep file.name]);
    eventNumbers = find(patIDsOfEvents==patIDs(iCrossval));
    firstEventInds = (n(eventNumbers(1))+1):n(eventNumbers(1)+1);
    testSetInds=[];
    for iEvent = 2:numel(eventNumbers)
        testSetInds=[testSetInds (n(eventNumbers(iEvent))+1):n(eventNumbers(iEvent)+1)];
    end
    imdb.images.set(firstEventInds)=1;
    imdb.images.set(testSetInds)=2;
        
%     mkdir('C:\Projects\SeizureDetection\data\networks\version7',['crossVal' num2str(iCrossval) 'ReTrain']);
%     copyfile([netDir filesep file.name],['C:\Projects\SeizureDetection\data\networks\version7' filesep 'crossVal' num2str(iCrossval) 'ReTrain']);
    opts.expDir = netDir;%['C:\Projects\SeizureDetection\data\networks\version7' filesep 'crossVal' num2str(iCrossval) 'ReTrain'];
    opts.train.expDir = netDir;
    opts.use15FPSpredecessor = false;
    opts.use10FramesWindow = true;
    opts.train.learningRate = 0.001 ;
    opts.train.numEpochs = 150 ; % 50 were trained before, now add 50 more
    [net, info]=cnn_szrdet(imdb,opts);
end
%% old training

% train in a random 80-20 cross validation manner
% n=imdb.meta.dbsizes;
% N=n(end);
% rng('default')
% randomIndices = randperm(numel(imdb.images.set));
% valSize = floor(numel(randomIndices)/5);
% for iCrossval=1:5
%     imdb.images.set = ones(1,N);
%     imdb.images.set(randomIndices((valSize*(iCrossval-1)+1):valSize*iCrossval)) = 2;
%     mkdir('C:\Projects\SeizureDetection\data\networks\versionUpperBound',['crossVal' num2str(iCrossval)]);
%     opts.expDir = ['C:\Projects\SeizureDetection\data\networks\versionUpperBound' filesep 'crossVal' num2str(iCrossval)];
%     opts.use15FPSpredecessor = false;
%     opts.use10FramesWindow = true;
%     [net, info]=cnn_szrdet(imdb,opts);
% end


%% validation and plots
N=n(end);
for iCrossval=6%:numel(n)
    imdb.images.set = 2*ones(1,N);
%     imdb.images.set((n(iCrossval-1)+1):n(iCrossval))=3;
%     netDir = ['C:\Projects\SeizureDetection\data\networks\versionUpperBound' filesep 'crossVal' num2str(iCrossval-1)];
%     file = dir([netDir filesep '*-100.mat']);
%     load([netDir filesep file.name]);
%     [~,minErrorIndex]=min(info.val.error);
%     file = dir([netDir filesep '*-' num2str(minErrorIndex) '.mat']);
%     load([netDir filesep file.name]);
%     imdb.images.set(imdb.meta.patientIDvec==patIDs(iCrossval))=3;
    imdb.images.set(testSetInds)=3;    
    Ntest = nnz(imdb.meta.valid10Frames(imdb.images.set == 3));
    testInds = find(boolean(imdb.meta.valid10Frames) & (imdb.images.set == 3));
%     Ntest = numel(testInds);
    testResults = cell(1,Ntest);
    net.layers{end}.type = 'softmax';
%     for i=1:Ntest
%         im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage;
%         im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage;
%         im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
%         im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
%         im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
%         im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
%         im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
%         im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
%         im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
%         im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
%         res=vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)));
%         softmaxOut = gather(res(end).x);
%         testResults{i}=softmaxOut;
%         disp(['spatio-temporal volume number ' num2str(i) ' tested!'])
%     end
%     testMeanImage = mean(imdb.images.data(:,:,:,testInds),4);
%     testStdDevImage = std(imdb.images.data(:,:,:,testInds),0,4);
%     testMeanImage = mean(imdb.images.data,4);
%     testStdDevImage = std(imdb.images.data,0,4);
%     for i=1:Ntest
%         im1 = (imdb.images.data(:,:,:,testInds(i))-testMeanImage)./testStdDevImage;
%         im2 = (imdb.images.data(:,:,:,testInds(i)-1)-testMeanImage)./testStdDevImage;
%         im3 = (imdb.images.data(:,:,:,testInds(i)-2)-testMeanImage)./testStdDevImage;
%         im4 = (imdb.images.data(:,:,:,testInds(i)-3)-testMeanImage)./testStdDevImage;
%         im5 = (imdb.images.data(:,:,:,testInds(i)-4)-testMeanImage)./testStdDevImage;
%         im6 = (imdb.images.data(:,:,:,testInds(i)-5)-testMeanImage)./testStdDevImage;
%         im7 = (imdb.images.data(:,:,:,testInds(i)-6)-testMeanImage)./testStdDevImage;
%         im8 = (imdb.images.data(:,:,:,testInds(i)-7)-testMeanImage)./testStdDevImage;
%         im9 = (imdb.images.data(:,:,:,testInds(i)-8)-testMeanImage)./testStdDevImage;
%         im10 = (imdb.images.data(:,:,:,testInds(i)-9)-testMeanImage)./testStdDevImage;
%         res=vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)));
%         softmaxOut = gather(res(end).x);
%         testResults{i}=softmaxOut;
%         disp(['spatio-temporal volume number ' num2str(i) ' tested!'])
%     end
%     meanImageIR = mean(reshape(imdb.images.data(:,:,1,testInds),1,[]));
%     meanImageDepth = mean(reshape(imdb.images.data(:,:,2,testInds),1,[]));
%     net.meta.meanImage = [meanImageIR meanImageDepth];
%     stdDevImageIR = std(reshape(imdb.images.data(:,:,1,testInds),1,[]));
%     stdDevImageDepth = std(reshape(imdb.images.data(:,:,2,testInds),1,[]));
%     net.meta.stdDevImage = [stdDevImageIR stdDevImageDepth];

% cleanTrainInds = trainSetInds(boolean(imdb.meta.clean10Frames(trainSetInds)));
% imdb.images.set(cleanTrainInds)=1;
% normalization on Test set does not bring nice results
% imdb.images.set(testSetInds)=1;

% Take the mean out
% meanImageIR = mean(reshape(imdb.images.data(:,:,1,imdb.images.set==1),1,[]));
% meanImageDepth = mean(reshape(imdb.images.data(:,:,2,imdb.images.set==1),1,[]));
% net.meta.meanImage = [meanImageIR meanImageDepth];

% divide by the standard variation [factor (1/(n-1))]
% stdDevImageIR = std(reshape(imdb.images.data(:,:,1,imdb.images.set==1),1,[]));
% stdDevImageDepth = std(reshape(imdb.images.data(:,:,2,imdb.images.set==1),1,[]));
% net.meta.stdDevImage = [stdDevImageIR stdDevImageDepth];
tic
for i=1:Ntest
        im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage;
        im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage;
%         im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
%         im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
%         im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
%         im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
%         im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
%         im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
%         im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
%         im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
%     im1 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im2 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-1),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im3 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-2),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im4 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-3),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im5 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-4),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im6 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-5),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im7 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-6),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im8 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-7),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im9 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-8),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     im10 = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data(:,:,:,testInds(i)-9),reshape(net.meta.meanImage,1,1,[])),reshape(net.meta.stdDevImage,1,1,[]));
%     res = vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
    res = vl_simplenn_scnseg(net, gpuArray(cat(3,im2,im1)), [], [], ...
    'disableDropout', true, ...
    'conserveMemory', true, ...
    'sync', false) ;
    softmaxOut = gather(res(end).x);
    testResults{i}=softmaxOut;
    disp(['spatio-temporal volume number ' num2str(i) ' tested!'])
end
disp(['Time per Frame: ' num2str(1000*toc/Ntest) 'ms.'])

    % output plot
    testResults = [testResults{:}];
    seizureProbability = squeeze(testResults(1,:,2));
    noSeizureProbability = squeeze(testResults(1,:,1));
    personsProbability = squeeze(testResults(2,:,2));
    noPersonsProbability = squeeze(testResults(2,:,1));
    gt = squeeze(imdb.images.labels(:,:,:,testInds));
    figure;

    % seizure GT
    subplot(4,1,1)
    hold on
    if any(gt(1,:)==2)
    bar(find(gt(1,:)==2),ones(1,nnz(gt(1,:)==2)),1,'green','edgecolor','none')
    end
    if any(gt(1,:)==1)
    bar(find(gt(1,:)==1),ones(1,nnz(gt(1,:)==1)),1,'red','edgecolor','none')
    end
    axis tight
    hold off
    title('Ground truth result for event "seizure"')
    % seizure test
    TPnum = 0;
    TNnum = 0;
    subplot(4,1,2)
    hold on
    if any(seizureProbability>noSeizureProbability)
    bar(find(seizureProbability>noSeizureProbability),ones(1,nnz(seizureProbability>noSeizureProbability)),1,'green','edgecolor','none')
    TPnum = sum(gt(1,:)==2 & seizureProbability>noSeizureProbability);
    end
    if any(seizureProbability<noSeizureProbability)
    bar(find(seizureProbability<noSeizureProbability),ones(1,nnz(seizureProbability<noSeizureProbability)),1,'red','edgecolor','none')
    TNnum = sum(gt(1,:)==1 & seizureProbability<noSeizureProbability);
    end
    plot(seizureProbability,'k')
    axis tight
    hold off
    accuracy = (TPnum + TNnum)/numel(testInds);
    title(['Test result for event "seizure", accuracy: ' num2str(accuracy)])
    
    % persons GT
    subplot(4,1,3)
    hold on
    if any(gt(2,:)==2)
    bar(find(gt(2,:)==2),ones(1,nnz(gt(2,:)==2)),1,'green','edgecolor','none')
    end
    if any(gt(2,:)==1)
    bar(find(gt(2,:)==1),ones(1,nnz(gt(2,:)==1)),1,'red','edgecolor','none')
    end
    axis tight
    hold off
    title('Ground truth result for event "persons in room"')
    % persons test
    TPnum = 0;
    TNnum = 0;
    subplot(4,1,4)
    hold on
    if any(personsProbability>noPersonsProbability)
    bar(find(personsProbability>noPersonsProbability),ones(1,nnz(personsProbability>noPersonsProbability)),1,'green','edgecolor','none')
    TPnum = sum(gt(2,:)==2 & personsProbability>noPersonsProbability);
    end
    if any(personsProbability<noPersonsProbability)
    bar(find(personsProbability<noPersonsProbability),ones(1,nnz(personsProbability<noPersonsProbability)),1,'red','edgecolor','none')
    TNnum = sum(gt(2,:)==1 & personsProbability<noPersonsProbability);
    end
    plot(personsProbability,'k')
    axis tight
    hold off
    accuracy = (TPnum + TNnum)/numel(testInds);
    title(['Test result for event "persons in room", accuracy: ' num2str(accuracy)])
    
    drawnow;
    save('IM1237_pixelwiseMeanSdevNet_epoch40_results','gt','testResults')
end

%% draw ROC curve


