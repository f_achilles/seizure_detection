% resultsCounter = 29;
modality = 'depth'; %'ir'; %
avgRecall = 0;
avgSpec = 0;
for networkType = 7

for LR = 10^(-3.25) %[10^(-2.5) 10^(-2.75) 10^(-3) 10^(-3.25) 10^(-3.5)] %[1e-1 1e-2 1e-3 1e-4] 

for fold = 1:5
    [recall, specificity] = calcSensAndSpec(modality, networkType, LR, fold, wdwSz);
    avgRecall = avgRecall+recall;
    avgSpec = avgSpec+specificity;
end
avgRecall = avgRecall/5;
avgSpec = avgSpec/5;

AUC = plotROCwithAUC(avgRecall,avgSpec);

end
end