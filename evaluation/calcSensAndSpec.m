function [sens, spec] = calcSensAndSpec(modality, networkType, cLR, fold, wdwSz)
if ~exist('wdwSz', 'var')
    wdwSz = 1;
end

netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_%s_Type%d_LR%.2f_fold%d_wdwSz5_shallow_meanImage_5foldCross',...
    modality,networkType,log(cLR)/log(10),fold);

% vars that are going to be used
testResults = [];
gt = [];
Ntest = [];
load(fullfile(netDir,sprintf('wdwSz%d_results_szOnly.mat',wdwSz)));

Ntest = numel(gt);
seizureProbability = squeeze(testResults(1,:,2));
threshold = 0:0.001:1.001;
accuracy    = zeros(1,numel(threshold));
precision   = zeros(1,numel(threshold));
recall      = zeros(1,numel(threshold));
specificity = zeros(1,numel(threshold));
for iThr=1:numel(threshold)
    TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
    TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
    accuracy(iThr)      = (TPnum + TNnum)/Ntest;
    precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
    recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
    specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
end
    
sens = recall;
spec = specificity;
end