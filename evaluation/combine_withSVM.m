%% the big 50-50 cross-Patient HoG+SVM test script

% load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat')
% diffSmpl = numel(imdb.meta.patientIDvec)-numel(imdb.meta.valid150Frames);
% imdb.meta.patientIDvec = imdb.meta.patientIDvec(1:(end-diffSmpl/2));

n=imdb.meta.dbsizes;
N=n(end);
patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};

cnnRoot = 'C:\Projects\SeizureDetection\data\networks\';

% clear imdb
for fold = 2 %1:2
    
    % training on set 1 means "fold 1"
    trainSetInds = [];
    trainPatIDs = patientsSets{fold};
    for pat = 1:numel(trainPatIDs)
        temp = find(imdb.meta.patientIDvec==trainPatIDs(pat));
        trainSetInds = cat(2,trainSetInds,temp);
    end
    % load IR-net
    load([cnnRoot sprintf('CrossVal_50_50_dropStart_infrared_Type4_LR-3.00_fold%d\\net-epoch-75.mat',fold)]);
    net.layers{end}.type = 'softmax';
    net = vl_simplenn_move(net,'gpu');
    IRnet = net;
    % load depth-net
    load([cnnRoot sprintf('CrossVal_50_50_dropStart_depth_Type4_LR-3.00_fold%d\\net-epoch-75.mat',fold)]);
    net.layers{end}.type = 'softmax';
    net = vl_simplenn_move(net,'gpu');
    Dnet = net;
    
    featuresIRD = zeros(N,2*64,'single'); %Depth only: zeros(N,4356,'single');
    for i=1:N
        % eval IR
        im1 = (imdb.images.data(:,:,1,i)-IRnet.meta.meanImage);
        IRres = vl_simplenn(IRnet, gpuArray(im1), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
        IRfc2out = gather(IRres(end-2).x);
        % eval D
        im1 = (imdb.images.data(:,:,2,i)-Dnet.meta.meanImage);
        Dres = vl_simplenn(Dnet, gpuArray(im1), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
        Dfc2out = gather(Dres(end-2).x);
%         [IRfeatureVector] = extractHOGFeatures(imdb.images.data(:,:,1,i));
%         [DfeatureVector] = extractHOGFeatures(imdb.images.data(:,:,2,i));
        featuresIRD(i,:) = single([reshape(IRfc2out,1,[]) reshape(Dfc2out,1,[])]);
        fprintf('Sample %d of %d processed.\n',i,N)
    end
    isSeizure = squeeze(imdb.images.labels(1,1,1,:))==2;
    patientIDvec = imdb.meta.patientIDvec;

    set                 = 3*ones(1,N);
    set(trainSetInds)   = 1;

	% balance the training set
    boolPinOldTrainSet =  isSeizure' & set==1;
    boolNinOldTrainSet = ~isSeizure' & set==1;
    numP = nnz(boolPinOldTrainSet);
    numN = nnz(boolNinOldTrainSet);
    if numP > numN
        % define randInds which are the ones of the overly represented set
        % that will be dicarded
        randInds = randperm(numP,numP-numN);
        PindsInOldTrainSet = find(boolPinOldTrainSet);
        set(PindsInOldTrainSet(randInds)) = 2;        
    elseif numP < numN
        % define randInds which are the ones of the overly represented set
        % that will be dicarded
        randInds = randperm(numN,numN-numP);
        NindsInOldTrainSet = find(boolNinOldTrainSet);
        set(NindsInOldTrainSet(randInds)) = 2; 
    else
        % is balanced
    end
    SVMModel = fitcsvm(featuresIRD(set==1,:),isSeizure(set==1));
    CompactSVMModel = compact(SVMModel);
    CompactSVMModel = fitPosterior(CompactSVMModel,featuresIRD(set==1,:),...
        isSeizure(set==1));
    [prediction,PostProbs] = predict(CompactSVMModel,featuresIRD(set==3,:));

    recall      = nnz(prediction&isSeizure(set==3))/nnz(isSeizure(set==3))
    % recall == sensitivity
    precision   = nnz(prediction&isSeizure(set==3))/nnz(prediction)
    specificity = nnz(~prediction&~isSeizure(set==3))/nnz(~isSeizure(set==3))
    
    % calculate AUC
    threshold = 0:0.001:1.001;
    recallCurve = zeros(1,numel(threshold));
    specifCurve = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(isSeizure(set==3) & PostProbs(:,2)>=threshold(iThr));
        TNnum = nnz(~isSeizure(set==3) & PostProbs(:,2)<threshold(iThr));
        recallCurve(iThr) = TPnum / (nnz(isSeizure(set==3))+eps);
        specifCurve(iThr) = TNnum / (nnz(~isSeizure(set==3))+eps);
    end
    % plot ROC
    ROCfig = figure; plot(1-specifCurve,recallCurve); axis equal; axis([0 1 0 1]);
    % sort, just in case
    [FPR,FPRSortInds] = sort(1-specifCurve);
    % calc AUC
    AUC=0;
    for a=1:(numel(FPR)-1)
        b=a+1;
        x=recallCurve(FPRSortInds(a));
        y=recallCurve(FPRSortInds(b));
        AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
    end
    AUC
    AUCs(fold) = AUC;
    mkdir('C:\Projects\SeizureDetection\workshop\SVMresults\combineFC2\');
    save(sprintf('C:\\Projects\\SeizureDetection\\workshop\\SVMresults\\combineFC2\\fold%d_results.mat',fold),...
        'isSeizure','prediction','recall','precision','specificity','set','PostProbs','AUC')
end

disp(AUCs)
disp(mean(AUCs))