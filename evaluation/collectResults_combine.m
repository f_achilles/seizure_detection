% Combine two network outputs with AND or OR
close all

ANDing = true;
ORing = false;
useDepth = true;
useIR = true;

resultsCounter = 1;

for wdwSize = [2 5 10]
for networkType = 4 %[2 4] %1:4

for cLR = 10^(-3.25) %[10^(-2.75) 10^(-3) 10^(-3.25) 10^(-3.5)] %[1e-1 1e-2 1e-3 1e-4] 10^(-2.5)
% vary learning rate   

for fold = 1:2
    
    if useDepth
    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_depth_Type%d_LR%.2f_fold%d_wdw%d',networkType,log(cLR)/log(10),fold,wdwSize);
    %     netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type%d_LR%.2f_fold%d',networkType,log(cLR)/log(10),fold);
    load(fullfile(netDir,sprintf('wdwSz%d_results_szOnly.mat',wdwSize)));
    
    seizureProbabilityDepth = squeeze(testResults(1,:,2));
    end
    
    if useIR
    netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type%d_LR%.2f_fold%d_wdw%d',networkType,log(cLR)/log(10),fold,wdwSize);
    %     netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type%d_LR%.2f_fold%d',networkType,log(cLR)/log(10),fold);
    load(fullfile(netDir,sprintf('wdwSz%d_results_szOnly.mat',wdwSize)));
    
    seizureProbabilityIR = squeeze(testResults(1,:,2));
    end
    
    if numel(seizureProbabilityDepth)~=numel(seizureProbabilityIR)
        error('what?')
        if fold == 1
            seizureProbabilityDepth(forbiddenTestSamplesFold1) = [];
        else
            seizureProbabilityDepth(forbiddenTestSamplesFold2) = [];
        end
    end
    
    if ANDing
        seizureProbability = seizureProbabilityIR.*seizureProbabilityDepth;
    elseif
        seizureProbability = (seizureProbabilityIR+seizureProbabilityDepth)/2;
    end
    
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end
    
    ROCfig = figure; plot(1-specificity,recall);
    % sort, just in case
    [FPR,FPRSortInds] = sort(1-specificity);
    % calc AUC
    AUC=0;
    for a=1:(numel(FPR)-1)
        b=a+1;
        x=recall(FPRSortInds(a));
        y=recall(FPRSortInds(b));
        AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
    end
    
    AUCs(resultsCounter,fold) = AUC;
        
    if fold == 2
        AUCs(resultsCounter,3) = mean(AUCs(resultsCounter,1:2));
    end
end
resultsCounter = resultsCounter+1
end
end
