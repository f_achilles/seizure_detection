% calculate CNN results for individual patients
patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};

%     load('C:\Projects\SeizureDetection\data\szDet_IMDB_with1239_LOG.mat');

%% begin patient-loop
for cPatientID = [1225, 1237, 11002]

    for fold = 1:2
        if any(cPatientID == patientsSets{fold})
            loadFold = 3-fold;
        end
    end
%% depth

networkFolder = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_depth_Type4_LR-3.00_fold%d',loadFold);
load(fullfile(networkFolder, 'net-epoch-75.mat'));

% change this to be dependent on the imdb.meta.patientIDvec
testInds = find(imdb.meta.patientIDvec == cPatientID);
% testInds = find(imdb.images.set == 2);

wdwSz = 1;

Ntest = numel(testInds);
testResults = cell(1,Ntest);
net.layers{end}.type = 'softmax';
net = vl_simplenn_move(net,'gpu');
% perform testing
im1=[];im2=[];im3=[];im4=[];im5=[];im6=[];im7=[];im8=[];im9=[];im10=[];
testIdxVec = 1:Ntest;
%     testIdxVec = 1:10000;
    for i=testIdxVec
        im1 = (imdb.images.data(:,:,2,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage(:,:,2);
%         im1 = imresize(im1,[224 224]);
        if wdwSz > 1
        im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage(:,:,2);
%         im2 = imresize(im2,[224 224]);
        end
        if wdwSz > 2
        im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
        im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
        im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 5
        im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
        im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
        im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
        im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
        im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        res = vl_simplenn(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', true, ...
        'sync', false) ;
%             net.layers{end}.class = imdb.images.labels(:,:,:,testInds(i)) ;
%             res = vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), 1, [], ...
%             'disableDropout', true, ...
%             'conserveMemory', false, ...
%             'sync', false) ;
        softmaxOut = gather(res(end).x);
        testResults{i}=softmaxOut;
        
        fprintf('sample %d of %d\n',i,Ntest)
    end
    %
    % calculate results
    testResults = [testResults{:}];
    seizureProbability = squeeze(testResults(1,:,2));
    seizureProbabilityDepth = seizureProbability;

    % store results
%     save([netDir filesep 'wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
 
    %% infrared
    
networkFolder = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\CrossVal_50_50_dropStart_infrared_Type4_LR-3.00_fold%d',loadFold);
load(fullfile(networkFolder, 'net-epoch-75.mat'));

% change this to be dependent on the imdb.meta.patientIDvec
% testInds = find(imdb.images.set == 2);

Ntest = numel(testInds);
testResults = cell(1,Ntest);
net.layers{end}.type = 'softmax';
net = vl_simplenn_move(net,'gpu');
% perform testing
im1=[];im2=[];im3=[];im4=[];im5=[];im6=[];im7=[];im8=[];im9=[];im10=[];
testIdxVec = 1:Ntest;
%     testIdxVec = 1:10000;
    for i=testIdxVec
        im1 = (imdb.images.data(:,:,1,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage(:,:,2);
%         im1 = imresize(im1,[224 224]);
        if wdwSz > 1
        im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage(:,:,2);
%         im2 = imresize(im2,[224 224]);
        end
        if wdwSz > 2
        im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
        im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
        im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        if wdwSz > 5
        im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
        im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
        im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
        im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
        im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
        end
        res = vl_simplenn(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', true, ...
        'sync', false) ;
%             net.layers{end}.class = imdb.images.labels(:,:,:,testInds(i)) ;
%             res = vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), 1, [], ...
%             'disableDropout', true, ...
%             'conserveMemory', false, ...
%             'sync', false) ;
        softmaxOut = gather(res(end).x);
        testResults{i}=softmaxOut;
        
        fprintf('sample %d of %d\n',i,Ntest)
    end
    %
    % calculate results
    testResults = [testResults{:}];
    seizureProbability = seizureProbabilityDepth .* squeeze(testResults(1,:,2));
%     noSeizureProbability = squeeze(testResults(1,:,1));
    gt = reshape(squeeze(imdb.images.labels(1,:,:,testInds)),1,[]);
    gt = gt(testIdxVec);
    threshold = 0:0.001:1.001;
    accuracy    = zeros(1,numel(threshold));
    precision   = zeros(1,numel(threshold));
    recall      = zeros(1,numel(threshold));
    specificity = zeros(1,numel(threshold));
    for iThr=1:numel(threshold)
        TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
        TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
        accuracy(iThr)      = (TPnum + TNnum)/Ntest;
        precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
        recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
        specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
    end

    % store results
%     save([netDir filesep 'wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
    
    %% draw ROC
    ROCfig = figure;
    FPR = 1-specificity;
    TPR = recall;
%     FPRraw = FPR;
%     TPRraw = TPR;
    [FPR,srtdInds] = sort(FPR);
    TPR = TPR(srtdInds);
    hold on
    hROC = plot(FPR,...
        TPR,...
        'g-', 'lineWidth',3,'markerSize',10);
    hold off

    % calc AUC
    AUC=0;
    for a=1:(numel(FPR)-1)
        b=a+1;
        x=TPR(a);
        y=TPR(b);
        AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
    end
    title(['ROC curve, AUC: ' num2str(AUC)])
    xlabel('1-specificity'); ylabel('sensitivity');

    axis equal
    axis([0 1 0 1])
    drawnow
%     pause
    
    
    %% end of patient-Loop
    
end