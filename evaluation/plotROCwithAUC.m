function AUC = plotROCwithAUC(recall,specificity)
% make ROC curve
[FPR,FPRSortInds] = sort(1-specificity);
ROCfig = figure; plot(FPR,recall(FPRSortInds));
% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=recall(FPRSortInds(a));
    y=recall(FPRSortInds(b));
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end

title(['ROC curve, AUC: ' num2str(AUC)])
xlabel('1-specificity'); ylabel('sensitivity');
axis equal
axis([0 1 0 1])
end
