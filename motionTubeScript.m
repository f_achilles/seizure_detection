%% read input video
videoPath = 'C:\Projects\SeizureDetection\data\person15_jogging_d1_uncomp.avi';
vidObj = VideoReader(videoPath);

nS = 10;
pL = 30;
nFrames = nS*pL;

vidMatrix = [];
for i=1:(nS*pL+2)
    cFrame = vidObj.readFrame;
    cFrame = imresize(single(rgb2gray(cFrame)),[100 100]);
    vidMatrix = cat(3,vidMatrix,cFrame);
end

imdb = [];
flow = cell(nFrames);
opticFlow = opticalFlowLKDoG();
magnitude = 0;
sampleResetCtr = 0;
iSample = 0;
for iFrame = 1:(nFrames+2)
    flow{iFrame} = estimateFlow(opticFlow, vidMatrix(:,:,iFrame));
    if iFrame > 2
        sampleResetCtr = sampleResetCtr+1;
        if sampleResetCtr == (pL+1)
%             sampleResetCtr = 0;
            sampleResetCtr = sampleResetCtr-1;
            VxCollector = [];
            VyCollector = [];
%             magnitude = 0;
            for iSF=0:(pL-1)
                 VxCollector = cat(3,VxCollector,flow{iFrame-pL+iSF}.Vx);
                 VyCollector = cat(3,VyCollector,flow{iFrame-pL+iSF}.Vy);
%                  magnitude = magnitude+flow{iFrame-pL+iSF}.Magnitude/pL;
            end
            iSample = iSample+1
            imdb.images.data(:,:,:,iSample) = cat(3,VxCollector,VyCollector);
%             imdb.images.labels(:,:,1,iSample) = magnitude;
            imdb.images.labels(:,:,:,iSample) = cat(3,flow{iFrame}.Vx,flow{iFrame}.Vy);
        end
    end
end
N=size(imdb.images.data,4);
imdb.images.set = [ones(1,floor(0.9*N)) 2*ones(1,N-floor(0.9*N))];

% define network
%%%
% motion tubes
%%%
net.layers = {} ;
% step 1: initialize the filters (if called for the first time)
f = 0.01;
% offset = 0.5;

param.P = 9;
param.n = 16;
param.T = 30;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,param.T*2,param.n, 'single'),...
                           'biases', zeros(1,param.n,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
net.layers{end+1} = struct('type', 'relu');
%%%
% scalar product convolution
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,param.n,2, 'single'),...
                           'biases', zeros(1,2,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 1) ;
%%%
% loss
%%%
net.layers{end+1} = struct('type', 'euclid') ;

netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\flowTubesTest1');
mkdir(netDir);
opts.expDir = netDir;
% start training
% opts.batchSize = 9;
[net,info] = cnn_train_f(net,imdb,@getBatch_f);

% visualize filters
figure
for iFilter = 1:param.n
    subplot(1,param.n,iFilter)
    imagesc(squeeze(net.layers{1}.filters(1,1,:,iFilter)));
end

% test after training
imdb.images.set = [ones(1,N)];
imdb.images.set(200)=2;

net.layers = net.layers(1:3);
res = vl_simplenn_f(net, imdb.images.data(:,:,:,imdb.images.set==2), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
output = res(end).x(:,:,1);
figure;
subplot(131)
imagesc(imdb.images.labels(:,:,1,imdb.images.set==2)); axis image;
title('original flow magnitude')
subplot(132)
imagesc(output); axis image;
title('estimate')
subplot(133)
imagesc(imdb.images.labels(:,:,1,imdb.images.set==2)-output); axis image;
title('difference original-estimate')

%% old training (discarded for now)
% inputMatrix = reshape(vidMatrix,[100 100 30 10]);
% % build up IMDB
% % imdb.images.data = inputMatrix;
% 
% flow = cell(1);
% for iSmpl = 10 %1:size(inputMatrix,4)
%     opticFlow = opticalFlowLKDoG();
%     magnitude = 0;
%     for iFrame = 1:size(inputMatrix,3)
%         flow{iFrame} = estimateFlow(opticFlow,gather(inputMatrix(:,:,iFrame,iSmpl)));
%         if iFrame > 2
%         magnitude = magnitude+flow{iFrame}.Magnitude/size(inputMatrix,3);
%         end
%     end
%     imdb.images.labels(:,:,1,iSmpl) = magnitude;
% end

figure
for i=3 %1:size(inputMatrix,3)
    subplot(121)
    imagesc(flow{i}.Magnitude); axis image
    subplot(122)
    imagesc(flow{i}.Vx); axis image
    pause(0.5)
end
figure; imagesc(magnitude.*(magnitude>0.025)); axis image
% defining the network
% Define a network

% input size 100x100
% normalize
% net.layers{end+1} = struct('type', 'normalize', ...
%                            'param', [5 1 0.0001/5 0.75]) ;
%%%
% motion tubes
%%%
net.layers = {} ;
% step 1: initialize the filters (if called for the first time)
f = 0.2;
offset = 0.5;

param.P = 9;
param.n = 16;
param.T = 30;

weights = cell(1);
weights{1} = f*(rand(2,1,param.T,param.n,'single')-0.5)+offset;

net.layers{end+1} = struct('type', 'tube', ...
                       'weights', cell(1), ...
                       'learningRate', 500, ...
                       'param', param) ;
net.layers{end}.weights = weights;

%%%
% scalar product convolution
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,param.n,1, 'single'),...
                           'biases', [], ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 0) ;
%%%
% loss
%%%
net.layers{end+1} = struct('type', 'euclid') ;

netDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\motionTubesTest3');
mkdir(netDir);
opts.expDir = netDir;
% start training
% opts.batchSize = 9;
[net,info] = cnn_train_f(net,imdb,@getBatch_f);

% test after training
net.layers = net.layers(1:2);
net = vl_simplenn_move(net,'gpu');
res = vl_simplenn_f(net, gpuArray(imdb.images.data(:,:,:,imdb.images.set==2)), [], [], ...
        'disableDropout', true, ...
        'conserveMemory', false, ...
        'sync', false) ;
output = gather(res(end).x);

% visualize the filters
discrFilterBank = gpuArray(zeros(param.P,param.P,param.T,param.n,'single'));
% squash inbetween 0.5 and P.4999, P being the width/heigth of each
% filter
Xdiscrete = squeeze(round(max(0,min(1,net.layers{1}.weights{1}(1,:,:,:)))*(param.P*0.99)+0.5));
Ydiscrete = squeeze(round(max(0,min(1,net.layers{1}.weights{1}(2,:,:,:)))*(param.P*0.99)+0.5));
discrFilterBank(sub2ind([param.P,param.P,param.T,param.n],...
    Ydiscrete,Xdiscrete,repmat((1:param.T)',1,param.n),repmat(1:param.n,param.T,1))) = 1/param.T;
figure;
for i=1:16
    subplot(4,4,i)
    vol3d('cdata',gather(discrFilterBank(:,:,:,i)),'texture','3D'); axis image vis3d; view(2)
end
weights_epoch214 = net.layers{1}.weights{1};
weights_epoch1 = net.layers{1}.weights{1};

min(weights_epoch214(:)-weights_epoch1(:))
%% experimental script for developing a new motion tube layer

forward = true;
backward = ~forward;
% input is a spatio-temporal volume (video), with dimensions NxMxT
% inputMatrix = randn(100,100,30,10,'single');
inputMatrix = reshape(vidMatrix,[100 100 30 10]);
sz =[size(inputMatrix,1) size(inputMatrix,2) size(inputMatrix,3) size(inputMatrix,4)];

% filter kernels are of dimension PxPxT, P taking typical values of 11, 9,
% 7, 5 or 3
%
% the aim is to decompose the input video into all moving objects (motion
% segmentation), and to describe this in a compact manner. Would an
% autoencoder not be exactly for that purpose?
%
% autoencoder scheme:
% - input -> n tube filters -> NxMxn feature map -> convolution -> fully
% connected -> compressed (encoded) vector
%
% - decoder: do all steps backwards, use the same weights (?), compare
% by L2-norm with input video
%
%
% objective function scheme:
% - the motion tubes are supposed to extract salient motion information
% from the image (not noise, etc), which is also what optical flow
% algorithms are supposed to do (removing noise by regularization). So
% ranging over T input frames we can compute T-1 optical flow maps.
% Averaging these maps might not be good, as oscillating motions would
% result in a zero average. We should rather extract absolute values that
% correspond to e.g. the strength of motion per pixel. This can then be
% retrieved from the NxMxn motion maps by convolution with a 1x1xnx1 kernel
% (scalar product). This kernel learns, which of the motion tubes has
% learnt what speed of motion to respond to. How should the output be
% normalized? Motion tubes should correspond to homogenuous patches that
% move through the scene. Hence, their output should be high (1?), if the
% patch they are seeing throughout T frames has little variance in all
% pixels. A suitable output would be 1/(1+var(px_i)), which is bounded in
% (0,1]. The scalar product-kernel would hence see these bounded values
% and aggregate them. The final output dimension could then actually be
% pixels/frame, so the motion speed derived from the optical flow
% algorithm.

param.P = 9;
param.n = 64;
param.T = 30;
param.firstRun = true;
% step 0: smoothen the input images with a gaussian kernel

% step 1: initialize the filters (if called for the first time)
f = 0.2;
offset = 0.5;
if param.firstRun
    % weights represent continuous x- and y- coordinate
    layer.weights{1} = f*(rand(2,1,param.T,param.n,'single')-0.5)+offset;
    param.firstRun = false;
end

% step 2: from single-valued x-y-positions, derive discrete px-coordinates
% inside PxP-kernel. create [0,1]-kernels, divide by T to normalize energy.
% store vector of locations (sub2ind) inside the PxPxT volume which are not
% 0, and their offset from the center.
discrFilterBank = zeros(param.P,param.P,param.T,param.n,'single');
% squash inbetween 0.5 and P.4999, P being the width/heigth of each
% filter
Xdiscrete = squeeze(round(max(0,min(1,layer.weights{1}(1,:,:,:)))*(param.P*0.99)+0.5));
Ydiscrete = squeeze(round(max(0,min(1,layer.weights{1}(2,:,:,:)))*(param.P*0.99)+0.5));
discrFilterBank(sub2ind([param.P,param.P,param.T,param.n],...
    Ydiscrete,Xdiscrete,repmat((1:param.T)',1,param.n),repmat(1:param.n,param.T,1))) = 1/param.T;

if forward
% step 3: use vl_nnconv to convolve discrete kernels and smoothed input
meanMap = vl_nnconv(inputMatrix,discrFilterBank,[],'Pad',floor(param.P/2));

% step 4: use the result of step 3 (which is the mean) and subtract it from
% each frame's corresponding pixels, using the indices-vector from step 2.
% now compute the square, sum, and divide by (T-1) for each pixel. Now use
% the formula 1/(1+var) in order to squash the result to a (0,1]-range.
shiftedMean = zeros(sz(1)+2*floor(param.P/2),sz(2)+2*floor(param.P/2),param.T,sz(4),param.n,'single');
shiftedMean((floor(param.P/2)+1):(end-floor(param.P/2)),(floor(param.P/2)+1):(end-floor(param.P/2)),:,:,:) = ...
    repmat(permute(meanMap,[1 2 5 4 3]),1,1,param.T,1,1);
shiftedMean = gpuArray(shiftedMean);
inputMatrix = gpuArray(inputMatrix);
discrFilterBank = gpuArray(discrFilterBank);

% tic
varianceMap = gpuArray(zeros(sz(1),sz(2),param.n,sz(4),'single'));
% varianceCell = cell(1);
for iFilter = 1:param.n
    tempShiftedMean = shiftedMean(:,:,:,:,iFilter);
    for iFrame = 1:param.T
%         shiftedMean(:,:,iFrame,:,iFilter) = circshift(shiftedMean(:,:,iFrame,:,iFilter),...
%             [Ydiscrete(iFrame,iFilter) Xdiscrete(iFrame,iFilter)]-floor(param.P/2)-1);
        
        tempShiftedMean(:,:,iFrame,:) = circshift(tempShiftedMean(:,:,iFrame,:),...
            [Ydiscrete(iFrame,iFilter) Xdiscrete(iFrame,iFilter)]-floor(param.P/2)-1);
    end
    meanFree{iFilter} = bsxfun(@minus,inputMatrix,tempShiftedMean((floor(param.P/2)+1):(end-floor(param.P/2)),(floor(param.P/2)+1):(end-floor(param.P/2)),:,:));
    meanFree{iFilter} = param.T*(meanFree{iFilter}.^2)/(max(1,param.T-1)); %variance, not meanFree, but same variable to save memory
    varianceMap(:,:,iFilter,:) = vl_nnconv(meanFree{iFilter},discrFilterBank(:,:,:,iFilter),[],'Pad',floor(param.P/2));
%     varianceCell{iFilter} = vl_nnconv(meanFree,discrFilterBank(:,:,:,iFilter),[],'Pad',floor(param.P/2));
end
% 15.8 seconds on CPU
% GPU:
% fastest: 4.0 seconds on GPU (Tesla K40c)
% (8.0 seconds with parfor in inner loop only(why?), 4.2 seconds with parfor in outer loop,
% 19 seconds when NOT copying parts of shiftedMean to tempShiftedMean)
% toc

% divide by variance, so to squash it inbetween (0,1]
Y = 1/(1+varianceMap);
clear tempShiftedMean shiftedMean meanMap varianceMap
end % end of forward
%%
% tic
backward = true;
if backward
dzdy = single(1);
% step 5: gradient!
dYdVar = -(Y.^2);
dzdVar = dzdy * dYdVar;
% dzdVarMap = gpuArray(zeros(sz(1),sz(2),param.n,sz(4),'single'));

dInputdXdir = (circshift(inputMatrix,[0 -1]) - circshift(inputMatrix,[0 +1]))/2;
dInputdYdir = (circshift(inputMatrix,[-1 0]) - circshift(inputMatrix,[+1 0]))/2;

dInputdXdir(:,1,:,:) = dInputdXdir(:,2,:,:);
dInputdXdir(:,end,:,:) = dInputdXdir(:,end-1,:,:);
dInputdYdir(1,:,:,:) = dInputdYdir(2,:,:,:);
dInputdYdir(end,:,:,:) = dInputdYdir(end-1,:,:,:);

Y = 0;
dzdf = gpuArray(zeros(2,1,param.T,param.n,'single'));
for iFilter = 1:param.n
    % compute the derivative filter-wise
    
    % we need the old meanFree(s)! persistent variable? each meanFree has
    % 12MB, so with 64 filter we have 768MB stored on GPU
    dzdVarMap = vl_nnconv(meanFree{iFilter},discrFilterBank(:,:,:,iFilter),[],dzdVar(:,:,iFilter,:),'Pad',floor(param.P/2));
    
    dVarMapdMeanFree = 2*param.T*meanFree{iFilter}/(max(1,param.T-1));
    dzdMeanFree = dVarMapdMeanFree .* dzdVarMap; % this is 100,100,30,10
    
    %dMeanFreedMeanMap = ?
%     dzdMeanMap = dMeanFreedMeanMap .* dzdMeanFree; % this needs to be 100,100,1,10
    dzdMeanMap = -sum(dzdMeanFree,3); %NO IDEA if this is correct, but interestingly all derivatives are positive here, does this make sense?
    
    dzdInput = vl_nnconv(inputMatrix,discrFilterBank(:,:,:,iFilter),[],dzdMeanMap,'Pad',floor(param.P/2));
    
    dzdXdir = dzdInput .* dInputdXdir;
    dzdYdir = dzdInput .* dInputdYdir;
    
    % aggregate directional derivatives for each frame
    dzdXFilter = mean(mean(dzdXdir,1),2);
    dzdYFilter = mean(mean(dzdYdir,1),2); %values are in range 1e-5 (if input is randn, hence mean-free)
    
    % aggregate derivative wrt input
    Y = Y+dzdInput/param.n;
    
    % aggregate derivative over all input samples
    dzdf(:,1,:,iFilter) = [ mean(dzdXFilter,4);
                            mean(dzdYFilter,4)];
end
    
end
% toc
% 4.3 seconds using GPU arrays (Tesla K40c)