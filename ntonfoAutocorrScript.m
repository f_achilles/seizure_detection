% Ntonfo et al 2012: Low-Complexity Image Processing for Real-Time
% Detection of Neonatal Clonic Seizures

%% find good threshold values visually
% define single time window
% 10s, 15FPS video: 150 frames
frameWdwSize = 150;
n = imdb.meta.dbsizes;
dataSize = size(imdb.images.data);
% first, compute the necessary binarisation threshold on a seizure
for iSequence = 1
    valid150FPSIdx = find(imdb.meta.valid15FPSvec((n(iSequence)+1):n(iSequence+1))) + n(iSequence);
    validSeizureIdx = valid150FPSIdx(imdb.images.labels(valid150FPSIdx) == 2);
    figure
    for iFrame = validSeizureIdx
        imagesc(imdb.images.data(:,:,1,iFrame));
        axis image
        drawnow
    end

    % frame-wise differences
    avgLumSignal = abs(diff(imdb.images.data(:,:,:,validSeizureIdx),1,4));
    % binarisation
    irThreshold = 20;
    depthThreshold = 20;
    figure
    for iFrame = 350:850
        subplot(1,2,1)
        imagesc(avgLumSignal(:,:,1,iFrame)>irThreshold);
        axis image
        subplot(1,2,2)
        imagesc(avgLumSignal(:,:,2,iFrame)>depthThreshold);
        axis image
        drawnow
    end

    % average
    avgLumSignal = sum(sum(avgLumSignal,1),2)/(dataSize(1)*dataSize(2));
    pause
end
%% calculate ACF-DF signal at each valid sample
% zTH is the threshold for finding "periodicity" in the autocorrelation
% signal
zTH = 0.2;
FPS = 15;
T_frame = 1/FPS;
irThreshold = 20;
depthThreshold = 20;
useIR = true;
useDepth = true;

% % append valid indices for a time window of 10 frames (!)
% valid150Frames = 0*imdb.meta.valid15FPSvec;
% for i = 150:n(end)
% if imdb.meta.valid15FPSvec((i-(150-2)):i)==ones(1,149);
%     valid150Frames(i)=1;
% end
% end
% % store the boolean vector that knows all the valid 10er-last frames
% imdb.meta.valid150Frames = valid150Frames;

% patientInd = 1;
n = imdb.meta.dbsizes;
%%%
% Create a storage cell in order to store all the resulting z-signals
%%%
ZStorage = cell(2,numel(n)-1);
% patientNames = [1226, 1237, 11982];
% trainValTestSplits = {{[2 4 6];[3];[1 5]},... %fair: [2 4 6];[3];[1 5]},...
%     {[2 4];[3];[1 5]},...
%     {[1 2 3 6 10 11 12 15 16 17 22];[4 14 21];[5 7 8 9 13 18 19 20]}};
% splits = trainValTestSplits{patientInd};

% patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};
patientsSets = {[11982  1239]; [1226   1237   10333  11982]};
for fold = 1 %:2
    trainSetInds = [];
    trainPatIDs = patientsSets{fold}; % trainPatIDs means testing set in this implementation. There is no training (yet)
    for pat = 1:numel(trainPatIDs)
        for iSeq = 1:(numel(n)-1)
            if imdb.meta.patientIDvec(n(iSeq)+1)==trainPatIDs(pat)
                trainSetInds = cat(2,trainSetInds,iSeq);
            end
        end
    end
    splits = trainSetInds;

    % zTHVec = 0.1:0.1:0.9;
%     ImageThrVec = 10:10:40;
    ImageThrVec = [10 20 40 60];
    for iImageThr = 1:4 %2; %
        irThreshold = ImageThrVec(iImageThr);
        depthThreshold = irThreshold;
    % for izTH = 8 %1:9
    %     zTH = zTHVec(izTH);
    for iSequence = 1:(numel(n)-1)
        if ~any(iSequence==splits)
            continue
        end
        fprintf('calculating ACF-DF for sequence %d (patient %d, threshold %d)...\n',iSequence,imdb.meta.patientIDvec(n(iSequence)+1),irThreshold)
        valid150FPSIdx = find(imdb.meta.valid15FPSvec((n(iSequence)+1):n(iSequence+1))) + n(iSequence);
        if isempty(valid150FPSIdx)
            warning(['Sequence ' num2str(iSequence) ' does not contain 150 subsequent frames, there was too much LAAAAAG!'])
            continue;
        else
            disp(['Sequence ' num2str(iSequence) ' contains 150 subsequent frames at ' num2str(numel(valid150FPSIdx)) ' locations.'])
        end
        valid150FPSIdx = valid150FPSIdx(150:end);
        seizureDetector = false(size(valid150FPSIdx));
        for iWindow = 1:numel(valid150FPSIdx)
            % frame-wise differences
            avgLumSignal = abs(diff(imdb.images.data(:,:,:,(valid150FPSIdx(iWindow)-149):valid150FPSIdx(iWindow)),1,4));
            % binarisation
            avgLumSignal = avgLumSignal>irThreshold;
            % erosion
            avgLumSignal = imerode(avgLumSignal,ones(3));
            % sum
            avgLumSignal = squeeze(sum(sum(avgLumSignal,1),2));
            % normalized autocorrelation
            autocorrSignal  = zeros(2,61);%0*avgLumSignal;
            CMNDFSignal     = ones(2,61);%ones(size(avgLumSignal));
            DFSignal        = zeros(2,61);%0*avgLumSignal;
            for iModality = 1:2
                for tau = 0:60 %(frameWdwSize-2)
                    autocorrSignal(iModality,tau+1) = dot(avgLumSignal(iModality,:),[avgLumSignal(iModality,(tau+1):end) avgLumSignal(iModality,1:tau)]);%zeros(1,tau)]);
                    DFSignal(iModality,tau+1) = sum((avgLumSignal(iModality,:)-[avgLumSignal(iModality,(tau+1):end) avgLumSignal(iModality,1:tau)]).^2);%zeros(1,tau)]).^2); %
                    if tau>0
                        CMNDFSignal(iModality,tau+1) = tau*DFSignal(iModality,tau+1)/sum(DFSignal(iModality,1:(tau+1)));
                    end
                end
                autocorrSignal(iModality,:) = autocorrSignal(iModality,:)/autocorrSignal(iModality,1);
            end
            zSignal = CMNDFSignal-autocorrSignal;
            ZStorage{fold,iSequence}.signal{iWindow} = zSignal;
            ZStorage{fold,iSequence}.imdbIndices = valid150FPSIdx;
    %         
    %         periodDuration = cell(1,2);
    %         for iModality = 1:2
    %             periodicInd = find(zSignal(iModality,:)<zTH);
    %             if ~isempty(periodicInd)
    %                 % compute duration of found period
    %                 periodDuration{iModality} = T_frame*(periodicInd-1);
    %             end
    %         end
    %         boolAllowedPeriod = false;
    %         if useIR
    %             boolAllowedPeriod = periodDuration{1} > 0.5 & periodDuration{1} < 3.5;
    %             seizureDetector(iWindow) = any(boolAllowedPeriod);
    %         end
    %         if useDepth
    %             boolAllowedPeriod = periodDuration{2} > 0.5 & periodDuration{2} < 3.5;
    %             seizureDetector(iWindow) = seizureDetector(iWindow) || any(boolAllowedPeriod);
    %         end
        end
    %     TPbool = seizureDetector' & squeeze(imdb.images.labels(valid150FPSIdx)) == 2;
    %     disp(['Sequence ' num2str(iSequence) ' done, Sensitivity: ' num2str(100*nnz(TPbool)/nnz(imdb.images.labels(valid150FPSIdx) == 2)) '%.'])
    %     SensitivityVec(iImageThr, izTH, iSequence) = 100*nnz(TPbool)/nnz(imdb.images.labels(valid150FPSIdx) == 2);
    %     
    %     TNnum = nnz(~seizureDetector' & squeeze(imdb.images.labels(valid150FPSIdx)) == 1);
    %     SpecificityVec(iImageThr, izTH, iSequence) = 100*TNnum / (nnz(imdb.images.labels(valid150FPSIdx)==1)+eps);
    %     disp(['Specificity: ' num2str(SpecificityVec(iImageThr, izTH, iSequence)) '%.'])
    end
    save(sprintf('ACF_DF_5050_patients11982_1239_threshold_%d.mat',irThreshold),'ZStorage')
    end
end
% end
% end

%% evaluation of the stored ZStorage autocorrelations

% load('C:\Projects\SeizureDetection\data\ZStoragePisani.mat')
ImageThrVec = [10 20 40 60];
for iImageThr = 1:4 %2; %
    irThreshold = ImageThrVec(iImageThr);
load(sprintf('ACF_DF_5050_patients11982_1239_threshold_%d.mat',irThreshold))

frameWdwSize = 150;
n = imdb.meta.dbsizes;
dataSize = size(imdb.images.data);

useIR = true;
useDepth = true;
FPS = 15;
T_frame = 1/FPS;

% patientNames = [1226, 1237, 11982];
% trainValTestSplits = {{[2 4 6];[3];[1 5]},... %fair: [2 4 6];[3];[1 5]},...
%     {[2 4];[3];[1 5]},...
%     {[1 2 3 6 10 11 12 15 16 17 22];[4 14 21];[5 7 8 9 13 18 19 20]}};
% splits = trainValTestSplits{patientInd};
% patientsSets = {[1207   1225   11002  1239]; [1226   1237   10333  11982]};
patientsSets = {[11982  1239]; [1226   1237   10333  11982]};

i = 0;
zTHVec = -1.01:0.01:2.01;
for fold = 1 %:2 %numel(patientsSets)
%     meanROCCollector = zeros(2,numel(zTHVec));
for iPat = patientsSets{fold} %test set is train set for now
    fprintf('Processing fold %d.\n',fold)
    ROCCollector = zeros(2,numel(zTHVec));
    TPPosTNNegCollector = zeros(4,numel(zTHVec));
for zInd = 1:numel(zTHVec)
    valuesPerPatient = [];
    zTH = zTHVec(zInd);
    fprintf('Processing fold %d, threshold %.2f\n',fold, zTH)
    valueCollector = zeros(4,numel(n)-1);
    i=0;
    for iSequence = 1:(numel(n)-1)
        % For each sequence of patient iPat, calculate the number of
        % Positives and True Positives, Negatives and True Negatives
        %
        % Do this for every threshold value!
        if ~(imdb.meta.patientIDvec(n(iSequence)+1) == iPat) || isempty(ZStorage{fold,iSequence})
            continue
        end
        
%         fprintf('Processing sequence %d, patient %d.\n',iSequence, imdb.meta.patientIDvec(n(iSequence)+1))
        
        valid150FPSIdx = ZStorage{fold,iSequence}.imdbIndices;
        seizureDetector = false(size(valid150FPSIdx));
        for iWindow = 1:numel(valid150FPSIdx)
            % go through all valid sample points in the video
            zSignal = ZStorage{fold,iSequence}.signal{iWindow};
            periodDuration = cell(1,2);
            for iModality = 1:2
                periodicInd = find(zSignal(iModality,:)<zTH);
                if ~isempty(periodicInd)
                    % compute duration of found period
                    periodDuration{iModality} = T_frame*(periodicInd-1);
                end
            end
            boolAllowedPeriod = false;
            if useIR
                boolAllowedPeriod = periodDuration{1} > 0.5 & periodDuration{1} < 3.5;
                seizureDetector(iWindow) = any(boolAllowedPeriod);
            end
            if useDepth
                boolAllowedPeriod = periodDuration{2} > 0.5 & periodDuration{2} < 3.5;
                seizureDetector(iWindow) = seizureDetector(iWindow) || any(boolAllowedPeriod); %use OR combination of modalities here
%                 seizureDetector(iWindow) = seizureDetector(iWindow) && any(boolAllowedPeriod); %use AND combination of modalities here
            end          
        end
        TPnum = nnz( seizureDetector' & squeeze(imdb.images.labels(valid150FPSIdx)) == 2);
        TNnum = nnz(~seizureDetector' & squeeze(imdb.images.labels(valid150FPSIdx)) == 1);
        PositiveNum = nnz(imdb.images.labels(valid150FPSIdx)==2)+eps;
        NegativeNum = nnz(imdb.images.labels(valid150FPSIdx)==1)+eps;
        valueCollector(:,iSequence) = [TPnum PositiveNum TNnum NegativeNum]';
    end
    % for each threshold value, sum over the P, TP and N, TN,
    % from this, calculate sensitivity and specificity over all sequences
    % of patient iPat
    %
    % This is not good! instead, the ValueCollector array should collect
    % TP,P,TN,N over the whole test set. Only then should sens and spec be
    % calculated and stored in ROCCollector(2,2002)
    %
    % #TODO!
    
%     valuesPerPatient = [valuesPerPatient valueCollector];
    
    sens = sum(valueCollector(1,:))/sum(valueCollector(2,:));
    spec = sum(valueCollector(3,:))/sum(valueCollector(4,:));
    ROCCollector(:,zInd) = [1-spec sens]';
    TPPosTNNegCollector(:,zInd) = sum(valueCollector,2);
end

% FPR = ROCCollector(1,:);
% % ROC curve
% % sort, just in case
% % [FPR,FPRSortInds] = sort(FPR);
% firstROCind = find(FPR==0,1,'first');
% if isempty(firstROCind)
%     firstROCind = 1;
% end
% lastROCind = find(FPR==1,1,'first');
% if isempty(lastROCind)
%     lastROCind = numel(FPR);
% end
% ROCfig2 = figure;
% hold on
% % hROC = plot(FPR(firstROCind:lastROCind),...
% %     ROCCollector(2,FPRSortInds(firstROCind:lastROCind)),...
% %     'r--', 'lineWidth',3,'markerSize',10);
% hROC = plot(FPR,...
%     [ROCCollector(2,:)],...
%     'r--', 'lineWidth',3,'markerSize',10);
% hold off
% % calc AUC
% % AUC=0;
% % for a=firstROCind:(lastROCind-1)
% %     b=a+1;
% %     x=ROCCollector(2,FPRSortInds(a));
% %     y=ROCCollector(2,FPRSortInds(b));
% %     AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
% % end
% 
% AUC=0;
% for a=1:(numel(FPR)-1)
%     b=a+1;
%     x=ROCCollector(2,a);
%     y=ROCCollector(2,b);
%     AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
% end
% title(['ROC curve patient ' num2str(iPat) ', AUC: ' num2str(AUC)])
% xlabel('1-specificity'); ylabel('sensitivity');
% % set(gca,'FontSize',20)
% axis equal
% axis([0 1 0 1])
% 
% save(['C:\Projects\SeizureDetection\workshop\PISresults' filesep 'Pisani' num2str(iPat)],'AUC','ROCCollector','FPR','TPPosTNNegCollector')
% saveas(gcf, ['C:\Projects\SeizureDetection\workshop\PISresults' filesep num2str(iPat) '_PisaniROCcurve_szOnly'], 'png');

% build the mean ROC over all patients individually. This is not good!
% meanROCCollector = meanROCCollector+ROCCollector;

FPR = ROCCollector(1,:);

% test
[FPRSrtd,FPRSortInds] = sort(FPR);


% ROC curve
ROCfig2 = figure;
hold on
hROC = plot(FPR,...
    [ROCCollector(2,:)],...
    'b--', 'lineWidth',3,'markerSize',10);
hold off
% calc AUC
AUC=0;
for a=1:(numel(FPR)-1)
    b=a+1;
    x=ROCCollector(2,a);
    y=ROCCollector(2,b);
    AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
end
title(['ROC curve of patient ' num2str(iPat) ', AUC: ' num2str(AUC)])
xlabel('1-specificity'); ylabel('sensitivity');
% set(gca,'FontSize',15)
axis equal
axis([0 1 0 1])

end % end of each patient per fold
% meanROCCollector = meanROCCollector/numel(patientsSets{fold});

% FPR = meanROCCollector(1,:);
% FPR = ROCCollector(1,:);
% 
% % test
% [FPRSrtd,FPRSortInds] = sort(FPR);
% 
% 
% % ROC curve
% ROCfig2 = figure;
% hold on
% hROC = plot(FPR,...
%     [ROCCollector(2,:)],...
%     'b--', 'lineWidth',3,'markerSize',10);
% hold off
% % calc AUC
% AUC=0;
% for a=1:(numel(FPR)-1)
%     b=a+1;
%     x=ROCCollector(2,a);
%     y=ROCCollector(2,b);
%     AUC = AUC + (FPR(b)-FPR(a))*(min([x,y]) + abs(x-y)/2);
% end
% title(['ROC curve of fold ' num2str(fold) ', AUC: ' num2str(AUC)])
% xlabel('1-specificity'); ylabel('sensitivity');
% % set(gca,'FontSize',15)
% axis equal
% axis([0 1 0 1])

% save(['C:\Projects\SeizureDetection\workshop\PISresults' filesep 'Pisani_Fold' num2str(fold)],'AUC','ROCCollector','FPR','TPPosTNNegCollector')
% saveas(gcf, ['C:\Projects\SeizureDetection\workshop\PISresults' filesep 'Fold_' num2str(fold) '_PisaniROCcurve_szOnly'], 'png');

end % end of the folds
end
% legend('CNN same-subject', 'CNN cross-subject', 'Pisani et al.', 'Location', 'SouthEast')
% legend('Pisani et al.', 'Location', 'SouthEast')

% set(gcf,'Position', [200 200 700 700])

%%
% FPRCollPisani = zeros(1,numel(zTHVec));
% recallCollPisani = zeros(1,numel(zTHVec));
% for patientInd = 1:3
% iPat = patientNames(patientInd);
% load(['C:\Projects\SeizureDetection\data\results' filesep 'Pisani' iPat{1}(3:end) '.mat'],'AUC','ROCCollector','FPR','TPPosTNNegCollector')
% FPR = ROCCollector(1,:);
% FPRCollPisani = FPRCollPisani + FPR;
% recallCollPisani = recallCollPisani + ROCCollector(2,:);
% % saveas(gcf, ['C:\Projects\SeizureDetection\data\results' filesep num2str(patientNames(patientInd)) '_PisaniROCcurve_szOnly'], 'png');
% end
% 
% % ROCfigPis = figure;
% hold on
% hROC = plot(FPRCollPisani/3,...
%             recallCollPisani/3,...
%             'r--', 'lineWidth',3,'markerSize',10);
% hold off
% 
% % legend('CNN same-subject', 'CNN cross-subject', 'Pisani et al.', 'Location', 'SouthEast')
% 
% legend('N = 1', 'N = 2', 'N = 5', 'N = 10', 'Pisani et al.', 'Location', 'SouthEast')
% set(gcf,'Position', [200 200 700 700])


