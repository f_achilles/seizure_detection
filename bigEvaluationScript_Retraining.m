%% the big Evaluation script / Re-Training

patientDataBases = {'seizureDetectionTrainingDataIRandDepthIM1226.mat',...
                    'seizureDetectionTrainingDataIRandDepthIM1237.mat',...
                    'seizureDetectionTrainingDataIRandDepthIM11982.mat'};
patientNames = {'IM1226', 'IM1237', 'IM11982'};
trainValTestSplits = {{[2 4 6];[3];[1 5]},... %fair: [2 4 6];[3];[1 5]},...
    {[2 4];[3];[1 2 3 4 5]},...%{[2 4];[3];[1 5]},...
    {[1 2 3 6 10 11 12 15 16 17 22];[4 14 21];[5 7 8 9 13 18 19 20]}};
for patientInd = 2 %2:numel(patientDataBases)
    load(['C:\Projects\SeizureDetection\data\' patientDataBases{patientInd}]);
    if patientInd == 2
        imdb.images.data=rot90(imdb.images.data,1);
    end
    netDir = ['C:\Projects\SeizureDetection\data\networks\validation\medianFilteredDepth\retrain' patientNames{patientInd}];
    mkdir(netDir);
    opts.expDir = netDir;
    opts.train.expDir = netDir;
    splits = trainValTestSplits{patientInd};
    trainSetInds = [];
    valSetInds = [];
    testSetInds = [];
    n=imdb.meta.dbsizes;
    % build train, val and test sets
    for iEvent = splits{1}
        trainSetInds=[trainSetInds (n(iEvent)+1):n(iEvent+1)];
    end
    for iEvent = splits{2}
        valSetInds=[valSetInds (n(iEvent)+1):n(iEvent+1)];
    end
    for iEvent = splits{3}
        testSetInds=[testSetInds (n(iEvent)+1):n(iEvent+1)];
    end
    
    % shape the IR data to reduce the high dynamic range   
    disp('Conversion to single precision...')
    imdb.images.data = single(imdb.images.data);
    disp('Logarithmic range conversion...')
    imdb.images.data(:,:,1,:) = log(1+imdb.images.data(:,:,1,:));
    % stretch to fit IR to the depth data distribution
    imdb.images.data(:,:,1,:) = 4500*imdb.images.data(:,:,1,:)/log(2^16);
    % squeeze IR and depth to be inside 0-255
    imdb.images.data = 255*imdb.images.data/4500;
    
    %perform median filtering on each depth frame
    N=n(end);
    tic
    disp('Median filtering...')
    for i=1:N
        imdb.images.data(:,:,2,i) = medfilt2(imdb.images.data(:,:,2,i));
    end
    disp(['Median filtering ' num2str(N) ' 100x100 16bit-images with [3x3] kernel takes ' num2str(toc) ' seconds.'])
    
    % divide in train, val, test
    imdb.images.set = 3*ones(1,N);
    imdb.images.set(trainSetInds)=1;
    imdb.images.set(valSetInds)=2;
    % single objective
    imdb.images.seizureLabels = imdb.images.labels(1,:,:,:);
    imdb.images.labels = imdb.images.seizureLabels;
    %
    % train on window sizes 1, 2, 5, 10
    for wdwSz = [1 2 5 10]
        opts.train.use15FPSpredecessor  = wdwSz == 2;
        opts.train.use5FramesWindow     = wdwSz == 5 ;
        opts.train.use10FramesWindow    = wdwSz == 10;
        opts.train.learningRate         = 0.0001 ;
        opts.train.numEpochs            = 150 ;
        opts.train.batchSize            = 50 ;
        %%%
        % specify the net to start ReTraining from
        %%%       
        pretrainNets = dir(['C:\Projects\SeizureDetection\data\networks\validation\medianFilteredDepth\unknown' patientNames{patientInd}(3:end) filesep patientNames{patientInd}(3:end) '_wdwSz' num2str(wdwSz) '_NETep*']);
        opts.train.pretrainNet = ['C:\Projects\SeizureDetection\data\networks\validation\medianFilteredDepth\unknown' patientNames{patientInd}(3:end) filesep pretrainNets(1).name];
        %%%
        % start training!
        %%%
        [net, info]=cnn_szrdet(imdb,opts);
        
        % choose best validation epoch for testing
        file = dir([netDir filesep '*-' num2str(opts.train.numEpochs) '.mat']);
        load([netDir filesep file.name]);
% the minimum validation error can be reached after Epoch 1 :/
        [~,minErrorIndex]=min(info.val.error);
        file = dir([netDir filesep '*-' num2str(minErrorIndex) '.mat']);
        load([netDir filesep file.name]);
        testInds = find(boolean(imdb.meta.valid10Frames) & (imdb.images.set == 3));
        Ntest = numel(testInds);
        testResults = cell(1,Ntest);
        net.layers{end}.type = 'softmax';
        % perform testing
        im1=[];im2=[];im3=[];im4=[];im5=[];im6=[];im7=[];im8=[];im9=[];im10=[];
        for i=1:Ntest
            im1 = (imdb.images.data(:,:,:,testInds(i))-net.meta.meanImage)./net.meta.stdDevImage;
            if wdwSz > 1
            im2 = (imdb.images.data(:,:,:,testInds(i)-1)-net.meta.meanImage)./net.meta.stdDevImage;
            end
            if wdwSz > 2
            im3 = (imdb.images.data(:,:,:,testInds(i)-2)-net.meta.meanImage)./net.meta.stdDevImage;
            im4 = (imdb.images.data(:,:,:,testInds(i)-3)-net.meta.meanImage)./net.meta.stdDevImage;
            im5 = (imdb.images.data(:,:,:,testInds(i)-4)-net.meta.meanImage)./net.meta.stdDevImage;
            end
            if wdwSz > 5
            im6 = (imdb.images.data(:,:,:,testInds(i)-5)-net.meta.meanImage)./net.meta.stdDevImage;
            im7 = (imdb.images.data(:,:,:,testInds(i)-6)-net.meta.meanImage)./net.meta.stdDevImage;
            im8 = (imdb.images.data(:,:,:,testInds(i)-7)-net.meta.meanImage)./net.meta.stdDevImage;
            im9 = (imdb.images.data(:,:,:,testInds(i)-8)-net.meta.meanImage)./net.meta.stdDevImage;
            im10 = (imdb.images.data(:,:,:,testInds(i)-9)-net.meta.meanImage)./net.meta.stdDevImage;
            end
            res = vl_simplenn_scnseg(net, gpuArray(cat(3,im10,im9,im8,im7,im6,im5,im4,im3,im2,im1)), [], [], ...
            'disableDropout', true, ...
            'conserveMemory', true, ...
            'sync', false) ;
            softmaxOut = gather(res(end).x);
            testResults{i}=softmaxOut;
        end
        % calculate results
        testResults = [testResults{:}];
        seizureProbability = squeeze(testResults(1,:,2));
        noSeizureProbability = squeeze(testResults(1,:,1));
        gt = reshape(squeeze(imdb.images.labels(:,:,:,testInds)),1,[]);
        threshold = 0:0.001:1.001;
        accuracy    = zeros(1,numel(threshold));
        precision   = zeros(1,numel(threshold));
        recall      = zeros(1,numel(threshold));
        specificity = zeros(1,numel(threshold));
        for iThr=1:numel(threshold)
            TPnum = nnz(gt(1,:)==2 & seizureProbability>=threshold(iThr));
            TNnum = nnz(gt(1,:)==1 & seizureProbability<threshold(iThr));
            accuracy(iThr)      = (TPnum + TNnum)/Ntest;
            precision(iThr)     = TPnum / (nnz(seizureProbability>=threshold(iThr))+eps);
            recall(iThr)        = TPnum / (nnz(gt(1,:)==2)+eps);
            specificity(iThr)   = TNnum / (nnz(gt(1,:)==1)+eps);
        end
        %%%
        % Draw PR-curve, ROC-curve, accuracy-curve and colored timeseries
        %%%
        drawResultCurves
        % store results
        save([netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_results_szOnly'],'gt','testResults');
        % clean up
        close all
        movefile([netDir filesep file.name],...
            [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_NETep' num2str(minErrorIndex) '.mat']);
        movefile([netDir filesep 'net-epoch-' num2str(opts.train.numEpochs) '.mat'],...
            [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_NETep' num2str(opts.train.numEpochs) '.mat']);
        movefile([netDir filesep 'net-train.pdf'],...
            [netDir filesep patientNames{patientInd} '_wdwSz' num2str(wdwSz) '_trainingPlot.pdf']);
        delete([netDir filesep 'net-epoch-*']);
    end
end