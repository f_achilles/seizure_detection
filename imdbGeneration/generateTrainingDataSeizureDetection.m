dbRoot = 'G:\KinectV2';
% dbRoot = 'C:\Projects\SeizureDetection\data';
% dbRoot = 'E:\EpilepsyData';


%% script to generate Training data for seizure detection
debug = false;
addKIR = true;
addKDPT = true;
% read in configuration CSV
csvpath = 'C:\Projects\SeizureDetection\data\ClassBorderFramesIM1226.csv';
% csvpath = 'C:\Projects\SeizureDetection\data\expansionPack_classBorderFrames.csv';
csvFileHandle = fopen(csvpath);
imdb = struct('images',[]);
% data, labels, set will be the sub-fields
datasetIdx = 0;
valid15FPS = [];
timestamps = [];
patientIDvec = uint16([]);
while ~feof(csvFileHandle)
    % skip first row
    cline = fgetl(csvFileHandle);
    if feof(csvFileHandle)
        break
    end
    datasetIdx = datasetIdx+1;
    
    % get folder of current patient database
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    databasepath = [dbRoot filesep splitResult{1}];
    % get current patient ID
    patientID = regexp(splitResult{1}, 'IM(\d+)-*(\d*)', 'tokens');
    patientID = str2double([patientID{1}{1} patientID{1}{2}]);
    % skip rows 3,4,5
    cline = fgetl(csvFileHandle);
    cline = fgetl(csvFileHandle);
    cline = fgetl(csvFileHandle);
    % get timestamp of first video
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    startVideoTime = splitResult{1};
    startVideoTime = regexp(startVideoTime, '(\d+)#\d+', 'tokens');
    startVideoTime = str2double(startVideoTime{1});
    % skip one row
    cline = fgetl(csvFileHandle);
    % get timestamp of last video
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    endVideoTime = splitResult{1};
    endVideoTime = regexp(endVideoTime, '(\d+)#\d+', 'tokens');
    endVideoTime = str2double(endVideoTime{1});
    % get indices of "seizure" label toggle frames
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    seizureToggleFrames = splitResult;
    % get indices of "persons" label toggle frames
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    personsToggleFrames = splitResult;
    % get indices of "patient" label toggle frames
    cline = fgetl(csvFileHandle);
    splitResult = regexp(cline, ';', 'split');
    patientToggleFrames = splitResult;
    % skip last row
    cline = fgetl(csvFileHandle);
    
    % generate ground truth label vectors and training image data
    kirFiles = dir([databasepath filesep '*.kir']);
    validFrameCell = cell(1,numel(kirFiles));
    videoCounter = 0;
    for i = 1:numel(kirFiles)
        % check if you are in the allowed range of timestamps
        [~,videoTimestamp] = fileparts(kirFiles(i).name);
        videoTimestamp = regexp(videoTimestamp, '(\d+)#\d+', 'tokens');
        videoTimestamp = str2double(videoTimestamp{:});
        if ~(videoTimestamp >= startVideoTime && ...
                videoTimestamp <= endVideoTime)
            continue;
        end
        videoCounter = videoCounter+1;
        % as soon as the timestamp is valid, we assume the *dir* command to
        % return a name-ordered list, which appears to be standard in
        % Windows! However enforce sorting by name (smallest to largest) if
        % errors occur.
        
        % read in full video
        if addKIR && addKDPT
            [irContainer,cTimestamps] = ...
                playKIR([databasepath filesep kirFiles(i).name]);
            [depthContainer,~] = ...
                playKDPT([databasepath filesep kirFiles(i).name(1:end-3) 'kdpt']);
        elseif addKIR
            [irContainer,cTimestamps] = ...
                playKIR([databasepath filesep kirFiles(i).name]);
        elseif addKDPT
            [depthContainer,cTimestamps] = ...
                playKDPT([databasepath filesep kirFiles(i).name(1:end-3) 'kdpt']);
        else
            error('no KIR or KDPT selected!')
        end
        
        % check for 15FPS-timestamps
        % over 33.333ms apart: not 30FPS, plus some safety margin (40ms)
        % 66.666ms apart: 15FPS, plus some safety margin (90ms)
        % store all indices of timestamps that have at least one
        % predecessor or successor in the range of 15FPS
        validFrames = cTimestamps*0;
        timeInd = 1;
        while timeInd < numel(cTimestamps)-2
            diffOne = cTimestamps(timeInd+1)-cTimestamps(timeInd);
            diffTwo = cTimestamps(timeInd+2)-cTimestamps(timeInd);
            if diffOne > 40 && diffOne < 90
                validFrames(timeInd)=1;
                validFrames(timeInd+1) = 1;
                timeInd = timeInd+1;
            elseif diffTwo > 40 && diffTwo < 90
                validFrames(timeInd)=1;
                validFrames(timeInd+2) = 1;
                timeInd = timeInd+2;
            else
                timeInd = timeInd+1;
            end
        end
        validFrames = boolean(validFrames);
        if any(validFrames)
        cTimestamps = cTimestamps(validFrames);
        % show timestep-differences for validation        
        if debug
        figure;
        plot(diff(cTimestamps))
        pause
        end
        timeDiffs = diff(cTimestamps);
        valid15FPSsecondImageIndices = timeDiffs>40 & timeDiffs<90;
        valid15FPS = [valid15FPS false valid15FPSsecondImageIndices];
        timestamps = [timestamps cTimestamps];
        patientIDvec = [patientIDvec uint16(0*cTimestamps)+patientID];
        % get rid of invalid non-15-FPS frames
        validFrameCell{videoCounter} = validFrames;
        downsampleRes = 200;
        if addKIR
        % crop input around the center
        irContainer = irContainer(:,(256-212+1):(256+212),validFrames);
        % resize the images to 100x100 resolution
        irContainer = interpn(single(irContainer), ...
            linspace(1,424,downsampleRes), ...
            (linspace(1,424,downsampleRes))',1:size(irContainer,3),'linear');
        end
        if addKDPT
        % crop input around the center
        depthContainer = depthContainer(:,(256-212+1):(256+212),validFrames);
        % resize the images to 100x100 resolution
        depthContainer = interpn(single(depthContainer), ...
            linspace(1,424,downsampleRes), ...
            (linspace(1,424,downsampleRes))',1:size(depthContainer,3),'linear');
        end
        
        if addKIR && addKDPT
            data{videoCounter} = cat(3,permute(irContainer,[1 2 4 3]), ...
                permute(depthContainer,[1 2 4 3]));
        elseif addKIR
            data{videoCounter} = permute(irContainer,[1 2 4 3]);
        elseif addKDPT
            data{videoCounter} = permute(depthContainer,[1 2 4 3]);
        else
            error('No input (KIR/KDPT) selected!')
        end
        end
    end
    % generate labels as 3xN vectors
    videoCounter = 1;
    seizureState = true;
    personsState = true;
    patientState = true;
    seizureFrameNr = 1;
    personsFrameNr = 1;
    patientFrameNr = 1;
    labels{videoCounter} = ones(3,numel(validFrameCell{videoCounter}));
    for i = 2:numel(seizureToggleFrames)
        if strcmp(seizureToggleFrames{i},'x')
            labels{videoCounter}(1,seizureFrameNr:end)=single(seizureState);
            labels{videoCounter}(2,personsFrameNr:end)=single(personsState);
            labels{videoCounter}(3,patientFrameNr:end)=single(patientState);
            labels{videoCounter} = labels{videoCounter}(:,validFrameCell{videoCounter});
            videoCounter = videoCounter+1;
            seizureState = true;
            personsState = true;
            patientState = true;
            seizureFrameNr = 1;
            personsFrameNr = 1;
            patientFrameNr = 1;
            labels{videoCounter} = ones(3,numel(validFrameCell{videoCounter}));
            continue;
        elseif ~isempty(seizureToggleFrames{i})
            % content gives the frame at which the variable toggles between
            % 0 and 1 (starts as 1 before first frame)
            newFrameNr = str2double(seizureToggleFrames{i});
            labels{videoCounter}(1,seizureFrameNr:(newFrameNr-1))=single(seizureState);
            seizureFrameNr = newFrameNr;
            seizureState = ~seizureState;
        end
        if ~isempty(personsToggleFrames{i})
            % content gives the frame at which the variable toggles between
            % 0 and 1 (starts as 1 before first frame)
            newFrameNr = str2double(personsToggleFrames{i});
            labels{videoCounter}(2,personsFrameNr:(newFrameNr-1))=single(personsState);
            personsFrameNr = newFrameNr;
            personsState = ~personsState;
        end
        if ~isempty(patientToggleFrames{i})
            % content gives the frame at which the variable toggles between
            % 0 and 1 (starts as 1 before first frame)
            newFrameNr = str2double(patientToggleFrames{i});
            labels{videoCounter}(3,patientFrameNr:(newFrameNr-1))=single(patientState);
            patientFrameNr = newFrameNr;
            patientState = ~patientState;
        end
    end
    labels{videoCounter}(1,seizureFrameNr:end)=single(seizureState);
    labels{videoCounter}(2,personsFrameNr:end)=single(personsState);
    labels{videoCounter}(3,patientFrameNr:end)=single(patientState);
    labels{videoCounter} = labels{videoCounter}(:,validFrameCell{videoCounter});
    
    % convert data{} and labels{} from cells to arrays
    n = zeros(1,videoCounter+1);
    for i=1:videoCounter
        n(i+1)=n(i)+size(labels{i},2);
    end
    N=n(end);
    dataPart{datasetIdx} = zeros(downsampleRes,downsampleRes,single(addKDPT+addKIR),N,'single');
    labelsPart{datasetIdx} = zeros(3,N,'single');
    for i=1:videoCounter
        dataPart{datasetIdx}(:,:,:,(n(i)+1):n(i+1))=data{i};
        labelsPart{datasetIdx}(:,(n(i)+1):n(i+1))=labels{i};
    end
    
    disp(['Hooray! ' databasepath ' is done!'])
end
clear data labels irContainer
% convert dataPart{} and labelsPart{} from cells to arrays
n = zeros(1,datasetIdx+1);
for i=1:datasetIdx
    n(i+1)=n(i)+size(labelsPart{i},2);
end
N=n(end);
imdb.images.data = zeros(downsampleRes,downsampleRes,single(addKDPT+addKIR),N,'single');
imdb.images.labels = zeros(3,N,'single');
for i=1:datasetIdx
    imdb.images.data(:,:,:,(n(i)+1):n(i+1))=dataPart{i};
    imdb.images.labels(:,(n(i)+1):n(i+1))=labelsPart{i};
end
% set labels to be either 1 or 2
imdb.images.labels = imdb.images.labels+1;
% propose the first n-1 datasets to be training and the last one for
% testing as a default
imdb.images.set = ones(1,N);
imdb.images.set((n(end-1)+1):n(end))=2;
% store the database sizes
imdb.meta.dbsizes=n;
% store the boolean vector that knows all the valid 15FPS second frames
imdb.meta.valid15FPSvec = valid15FPS;
% store the boolean vector that knows all the valid 15FPS second frames
imdb.meta.timestamps = timestamps;
% close the .csv file
fclose(csvFileHandle);

imdb.images.labelsOld = imdb.images.labels;
imdb.images.labels = imdb.images.labels(1:2,:,:,:);
imdb.images.labels = permute(imdb.images.labels,[1 3 4 2]);
    
% append valid indices for a time window of 10 frames (!)
valid10Frames = 0*valid15FPS;
for i = 10:N
if valid15FPS((i-8):i)==ones(1,9);
    valid10Frames(i)=1;
end
end
% store the boolean vector that knows all the valid 10er-last frames
imdb.meta.valid10Frames = valid10Frames;

% append valid indices for a time window of 5 frames (!)
valid5Frames = 0*imdb.meta.valid15FPSvec;
for i = 5:N
if imdb.meta.valid15FPSvec((i-5+2):i)==ones(1,5-1);
    valid5Frames(i)=1;
end
end
% store the boolean vector that knows all the valid 10er-last frames
imdb.meta.valid5Frames = valid5Frames;


% append valid indices for a clean time window of 10 frames (all labels are the same)
%
% but what does that mean? We used this in order to clearly separate
% between (purely) positive and purely negative samples, such that we
% provide the easiest training data to the network. However the gains are
% minimal, such that the "unclean" valid10Frames are fine as well
clean10Frames = 0*imdb.meta.valid10Frames;
for i = find(imdb.meta.valid10Frames)
if bsxfun(@eq,imdb.images.labels(:,:,:,(i-9):(i-1)),imdb.images.labels(:,:,:,i))
    clean10Frames(i)=1;
end
end
% store the boolean vector that knows all the valid 10er-last frames that
% only have one constant label
imdb.meta.clean10Frames = clean10Frames;

% for every frame, add the patient ID, so that the frames can be grouped
% for leave-one-out testing
imdb.meta.patientIDvec = patientIDvec;


%%
error('Stop here!')

%% merging of two imdb databases
n_new = imdb.meta.dbsizes;
%forbidden patients: 11982 and 1226
for i=2:(numel(imdb.meta.dbsizes))
if imdb.meta.patientIDvec(imdb.meta.dbsizes(i)) ~= 1226 && imdb.meta.patientIDvec(imdb.meta.dbsizes(i)) ~= 11982
    % all good
else
    n_new(i:end) = n_new(i:end)-(n_new(i)-n_new(i-1));
end
end
n_new = unique(n_new);
if n_new(end) ~= size(imdbAll.images.data,4) || n_new(1) ~= 0
    error('new n is wrong!')
end
imdbAll.meta.dbsizes = n_new;

imdbAll.meta.patientIDvec = imdb.meta.patientIDvec(:,imdb.meta.patientIDvec ~= 11982 & imdb.meta.patientIDvec ~=1226);

% now append the next one
imdbAll = imdbAppend(imdbAll, imdb);



