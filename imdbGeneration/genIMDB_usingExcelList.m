dbRoot = 'H:\BackupOf3TB_KinectExport\ExportedKinectData';
% dbRoot = 'C:\Projects\SeizureDetection\data';
% dbRoot = 'E:\EpilepsyData';
downsampleRes = 424/4;
nSubRow = 4;
numPatches = nSubRow^2;
%% script to generate Video data for Supplemetary Material
debug = false;
addKIR = true;
addKDPT = false;
% read in configuration CSV
excelpath = 'C:\Projects\SeizureDetection\data\KinectV2_testList.xls';
[~,~,xlsData] = xlsread(excelpath);
% how to read the dates stored in the database
% daytime = datevec(datestr(rawXlsData{1,5},13));
% daydate = datevec(rawXlsData{1,4},'dd.mm.yyyy');
% fulldate = cat(2,daydate(1:3),daytime(4:6));% elements 4:6 resemble hours, minutes and
% seconds

% csvFileHandle = fopen(excelpath);
imdb = struct('images',[]);
% data, labels, set will be the sub-fields
datasetIdx = 0;
valid15FPS = [];
timestamps = [];
patientIDvec = uint16([]);

for cLine = 1:3 %:size(xlsData,1)
    % skip empty lines
    if isnan(xlsData{cLine,3}), continue; end %actually checks for missing seizure number
    datasetIdx = datasetIdx+1;
    % create clinical Begin {:,5} and End-times {:,7} in Porto format
    % clinical begin
    daytime = datevec(datestr(xlsData{cLine,5},13));
    daydate = datevec(xlsData{cLine,4},'dd.mm.yyyy');
    fulldate = cat(2,daydate(1:3),daytime(4:6));
    beginDate = datenum(fulldate);
    clinBeginStr = num2str(fulldate(1));
    for iDateEl = 2:6
        if fulldate(iDateEl) < 10
            clinBeginStr = [clinBeginStr '0' num2str(fulldate(iDateEl))];
        else
            clinBeginStr = [clinBeginStr num2str(fulldate(iDateEl))];
        end
    end
    clinBeginStr = [clinBeginStr '000'];
    % clinical end
    daytime = datevec(datestr(xlsData{cLine,7},13));
    daydate = datevec(xlsData{cLine,4},'dd.mm.yyyy');
    fulldate = cat(2,daydate(1:3),daytime(4:6));
    endDate = datenum(fulldate);
    clinEndStr = num2str(fulldate(1));
    for iDateEl = 2:6
        if fulldate(iDateEl) < 10
            clinEndStr = [clinEndStr '0' num2str(fulldate(iDateEl))];
        else
            clinEndStr = [clinEndStr num2str(fulldate(iDateEl))];
        end
    end
    clinEndStr = [clinEndStr '000'];
    
    
    % create path to read seizure data from
    % get current patient ID
    patientID = regexp(xlsData{cLine,2}, 'IM (\d+)-*(\d*)', 'tokens');
    % get seizure number
    szNumber = xlsData{cLine,3};
    % create dirname
    dirname = ['IM' patientID{1}{1}];
    if ~isempty(patientID{1}{2})
        dirname = [dirname '-' patientID{1}{2}];
    end
    if szNumber < 10
        dirname = [dirname '_sz0' num2str(szNumber)];
    else
        dirname = [dirname '_sz' num2str(szNumber)];
    end
    dirname = [dirname '_kinect'];
    databasepath = [dbRoot filesep dirname];
    
    % read in data
    kirFiles = dir([databasepath filesep '*.kir']);
    kdptFiles = dir([databasepath filesep '*.kdpt']);
    if numel(kirFiles) ~= numel(kdptFiles)
        warning('A different number of KDPT and KRI files were found! Continuing at your own risk...')
    end
    data = cell(1);
    labels = cell(1);
    for iVid = 1:numel(kirFiles)
        % read
        if addKIR
        [irContainer,irTimestamps] = ...
            playKIR([databasepath filesep kirFiles(iVid).name],-1);
        cTimestamps = irTimestamps;
        end
        if addKDPT
        [depthContainer,depthTimestamps] = ...
            playKDPT([databasepath filesep kdptFiles(iVid).name]);
        cTimestamps = depthTimestamps;
        end
        if addKIR && addKDPT
            % deal with unmatching timestamps in KDPT and KIR
            [minN,minInd] = min([numel(irTimestamps) numel(depthTimestamps)]);
            validSmallerFrames = [];
            validBiggerFrames = [];
            if minInd == 1
                smallerVector = irTimestamps;
                biggerVector = depthTimestamps;
            else
                smallerVector = depthTimestamps;
                biggerVector = irTimestamps;
            end
            for iFrame = 1:minN
                matchInd = find(smallerVector(iFrame)==biggerVector);
                if ~isempty(matchInd)
                    validSmallerFrames = [validSmallerFrames iFrame];
                    validBiggerFrames = [validBiggerFrames matchInd];
                end
            end
            if isempty(validSmallerFrames)
                continue;
            end
            cTimestamps = smallerVector(validSmallerFrames);
            if minInd == 1
                irContainer = irContainer(:,:,validSmallerFrames);
                depthContainer = depthContainer(:,:,validBiggerFrames);
            else
                depthContainer = depthContainer(:,:,validSmallerFrames);
                irContainer = irContainer(:,:,validBiggerFrames);
            end
        end
        % convert timestamps into milliseconds
        % assumption: the longest change possible is one day, (but actually
        % that is not correct, what if that day is at the end of the month?
        % ahrg
        cTS = num2str(uint64(cTimestamps(:)));
        cTS = (datenum(cTS,'yyyymmddHHMMSSFFF'))';
        
        validFrames = cTS*0;
        timeInd = 1;
        while timeInd < numel(cTS)-2
            diffOne = datevec(cTS(timeInd+1)-cTS(timeInd));
            diffTwo = datevec(cTS(timeInd+2)-cTS(timeInd));
            diffOne = 1000*diffOne(6);
            diffTwo = 1000*diffTwo(6);
            if diffOne > 40 && diffOne < 90
                validFrames(timeInd)=1;
                validFrames(timeInd+1) = 1;
                timeInd = timeInd+1;
            elseif diffTwo > 40 && diffTwo < 90
                validFrames(timeInd)=1;
                validFrames(timeInd+2) = 1;
                timeInd = timeInd+2;
            else
                timeInd = timeInd+1;
            end
        end
        validFrames = boolean(validFrames);
        if any(validFrames)
            cTS = cTS(validFrames);
            % show timestep-differences for validation        
            if debug
                figure;
                naiveDiff = diff(cTimestamps);
                betterDiff = datevec(diff(cTS),'FFF');
                betterDiff = betterDiff(:,6);
                plot(naiveDiff,'b')
                hold on
                plot(betterDiff(:)*1000,'r')
                hold off
                pause
            end
            timeDiffs = datevec(diff(cTS));
            timeDiffs = 1000*timeDiffs(:,6);
            valid15FPSsecondImageIndices = timeDiffs'>40 & timeDiffs'<90;
            valid15FPS = [valid15FPS false valid15FPSsecondImageIndices];
            timestamps = [timestamps cTS];
            if isempty(patientID{1}{2})
                patientIDvec = [patientIDvec uint16(0*cTS)+uint16(str2double(patientID{1}{1}))];
            else
                patientIDvec = [patientIDvec uint16(0*cTS)+uint16(str2double([patientID{1}{1} patientID{1}{2}]))];
            end
            % get rid of invalid non-15-FPS frames
            validFrameCell{iVid} = validFrames;

            if addKIR
                % crop input around the center
                irContainer = irContainer(:,(256-212+1):(256+212),validFrames);
                % rotate image so that patient is upright
                irContainer = rot90(single(irContainer),1);
                % map over-saturated regions to zero
                irContainer(irContainer==(2^16-1))=0;
                % IR-image normalization
                irContainer = 255*log(1+irContainer)/log(2^16);
                % extract smaller patches
                subPatches = cell(1,numPatches);
                eLen = (424/nSubRow);
                for iPatch=1:numPatches
                    rS = eLen*mod(iPatch-1+nSubRow,nSubRow)+1;
                    rE = eLen*(mod(iPatch-1+nSubRow,nSubRow)+1);
                    cS = eLen*floor((iPatch-1)/nSubRow)+1;
                    cE = eLen*(floor((iPatch-1)/nSubRow)+1);
                    subPatches{iPatch} = irContainer(rS:rE,cS:cE,:);
                end
                % resize the images to lower resolution
                irContainer = imresize(irContainer,[downsampleRes downsampleRes],'bilinear');
            end
            if addKDPT
                % crop input around the center
                depthContainer = depthContainer(:,(256-212+1):(256+212),validFrames);
                % rotate image so that patient is upright
                depthContainer = rot90(single(depthContainer),1);
                % median smoothing to remove sensor noise (esp. at geom.
                % edges)
                for iFrame=1:size(depthContainer,3)
                    depthContainer(:,:,iFrame) = medfilt2(depthContainer(:,:,iFrame),[5 5]);
                end
                % Depth-images normalization
                depthContainer = 255*depthContainer/4500;
                % extract smaller patches
                subPatches = cell(1,numPatches);
                eLen = (424/nSubRow);
                for iPatch=1:numPatches
                    rS = eLen*mod(iPatch-1+nSubRow,nSubRow)+1;
                    rE = eLen*(mod(iPatch-1+nSubRow,nSubRow)+1);
                    cS = eLen*floor((iPatch-1)/nSubRow)+1;
                    cE = eLen*(floor((iPatch-1)/nSubRow)+1);
                    subPatches{iPatch} = depthContainer(rS:rE,cS:cE,:);
                end
                % resize the images to lower resolution
                depthContainer = imresize(depthContainer,[downsampleRes downsampleRes],'bilinear');
            end

            if addKIR && addKDPT
                data{iVid} = cat(3,permute(irContainer,[1 2 4 3]), ...
                    permute(depthContainer,[1 2 4 3]));
                error('Stacking IR and Depth is out of fashion!')
            elseif addKIR
                data{iVid} = permute(irContainer,[1 2 4 3]);
                for iPatch=1:numPatches
                    data{iVid} = cat(3,data{iVid},permute(subPatches{iPatch},[1 2 4 3]));
                end
            elseif addKDPT
                data{iVid} = permute(depthContainer,[1 2 4 3]);
                for iPatch=1:numPatches
                    data{iVid} = cat(3,data{iVid},permute(subPatches{iPatch},[1 2 4 3]));
                end
            else
                error('No input (KIR/KDPT) selected!')
            end
        end
        % generate labels
        labels{iVid} = ones(1,nnz(validFrameCell{iVid}),'uint8');
        for iValFrame=1:nnz(validFrameCell{iVid})
            if cTS(iValFrame) >= beginDate && cTS(iValFrame) < endDate
                labels{iVid}(iValFrame) = 2;
            end
        end
    end
    
    % convert data{} and labels{} from cells to arrays
    n = zeros(1,numel(kirFiles)+1);
    for i=1:numel(kirFiles)
        n(i+1)=n(i)+size(labels{i},2);
    end
    N=n(end);
    dataPart{datasetIdx} = zeros(downsampleRes,downsampleRes,numPatches+single(addKDPT)+single(addKIR),N,'single');
    labelsPart{datasetIdx} = zeros(1,N,'single');
    for i=1:numel(kirFiles)
        dataPart{datasetIdx}(:,:,:,(n(i)+1):n(i+1)) =data{i};
        labelsPart{datasetIdx}(:,(n(i)+1):n(i+1))   =labels{i};
    end
    clear data labels irContainer depthContainer subPatches
    disp(['Hooray! ' databasepath ' is done!'])
end
%
% convert dataPart{} and labelsPart{} to form the imdb struct
n = zeros(1,datasetIdx+1);
for i=1:datasetIdx
    n(i+1)=n(i)+size(labelsPart{i},2);
end
N=n(end);
imdb.images.data = zeros(downsampleRes,downsampleRes,numPatches+single(addKDPT)+(addKIR),N,'single');
imdb.images.labels = zeros(1,N,'single');
for i=1:datasetIdx
    imdb.images.data(:,:,:,(n(i)+1):n(i+1))=dataPart{i};
    imdb.images.labels(:,(n(i)+1):n(i+1))=labelsPart{i};
end
clear dataPart labelsPart

% propose the first n-1 datasets to be training and the last one for
% testing as a default
imdb.images.set = ones(1,N);
imdb.images.set((n(end-1)+1):n(end))=2;
% store the database sizes
imdb.meta.dbsizes=n;
% store the boolean vector that knows all the valid 15FPS second frames
imdb.meta.valid15FPSvec = valid15FPS;
% store the boolean vector that knows all the valid 15FPS second frames
imdb.meta.timestamps = timestamps;

imdb.images.labels = permute(imdb.images.labels,[1 3 4 2]);
    
% % append valid indices for a time window of 10 frames (!)
% valid10Frames = 0*valid15FPS;
% for i = 10:N
% if valid15FPS((i-8):i)==ones(1,9);
%     valid10Frames(i)=1;
% end
% end
% % store the boolean vector that knows all the valid 10er-last frames
% imdb.meta.valid10Frames = valid10Frames;
% 
% % append valid indices for a clean time window of 10 frames (all labels are the same)
% clean10Frames = 0*imdb.meta.valid10Frames;
% for i = find(imdb.meta.valid10Frames)
% if bsxfun(@eq,imdb.images.labels(:,:,:,(i-9):(i-1)),imdb.images.labels(:,:,:,i))
%     clean10Frames(i)=1;
% end
% end
% % store the boolean vector that knows all the valid 10er-last frames that
% % only have one constant label
% imdb.meta.clean10Frames = clean10Frames;

% for every frame, add the patient ID, so that the frames can be grouped
% for leave-one-out testing
imdb.meta.patientIDvec = patientIDvec;


%%
error('Stop here!')

%% merging of two imdb databases
n_new = imdb.meta.dbsizes;
%forbidden patients: 11982 and 1226
for i=2:(numel(imdb.meta.dbsizes))
if imdb.meta.patientIDvec(imdb.meta.dbsizes(i)) ~= 1226 && imdb.meta.patientIDvec(imdb.meta.dbsizes(i)) ~= 11982
    % all good
else
    n_new(i:end) = n_new(i:end)-(n_new(i)-n_new(i-1));
end
end
n_new = unique(n_new);
if n_new(end) ~= size(imdbAll.images.data,4) || n_new(1) ~= 0
    error('new n is wrong!')
end
imdbAll.meta.dbsizes = n_new;

imdbAll.meta.patientIDvec = imdb.meta.patientIDvec(:,imdb.meta.patientIDvec ~= 11982 & imdb.meta.patientIDvec ~=1226);

% now append the next one
imdbAll = imdbAppend(imdbAll, imdb);

imdbAll.meta.patientIDvec   = cat(2, imdbAll.meta.patientIDvec, imdb.meta.patientIDvec);



