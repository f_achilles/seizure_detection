function imdbAll = imdbAppend(imdbAll, imdb)

imdbAll.images.data     = cat(4, imdbAll.images.data, imdb.images.data);
imdbAll.images.labels   = cat(4, imdbAll.images.labels, imdb.images.labels);
imdbAll.images.labelsOld= cat(2, imdbAll.images.labelsOld , imdb.images.labelsOld);
imdbAll.images.set      = cat(2, imdbAll.images.set, imdb.images.set);

imdbAll.meta.clean10Frames  = cat(2, imdbAll.meta.clean10Frames, imdb.meta.clean10Frames);
imdbAll.meta.dbsizes        = cat(2, imdbAll.meta.dbsizes, imdb.meta.dbsizes(2:end)+imdbAll.meta.dbsizes(end));
imdbAll.meta.patientIDvec   = cat(2, imdbAll.meta.patientIDvec, imdb.meta.patientIDvec);
imdbAll.meta.timestamps     = cat(2, imdbAll.meta.timestamps, imdb.meta.timestamps);
imdbAll.meta.valid10Frames  = cat(2, imdbAll.meta.valid10Frames, imdb.meta.valid10Frames);
imdbAll.meta.valid15FPSvec  = cat(2, imdbAll.meta.valid15FPSvec, imdb.meta.valid15FPSvec);

end